define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/_base/array',
  'entryscape-commons/defaults',
  'dijit/_WidgetBase',
], (declare, lang, domConstruct, domAttr, domClass, array, defaults, _WidgetBase) =>

  declare([_WidgetBase], {
    buildRendering() {
      this.domNode = this.srcNodeRef || domConstruct.create('div');
      domClass.add(this.domNode, 'list-group');
    },

    show() {
      this.inherited(arguments);
      const rdfutils = defaults.get('rdfutils');
      const catalogs = [];
      const query = defaults.get('entrystore')
        .newSolrQuery().rdfType('dcat:Catalog')
        .uriProperty('dcat:dataset', '*');
      query.list().forEach((catalogEntry) => {
        catalogs.push({
          entry: catalogEntry,
          label: rdfutils.getLabel(catalogEntry),
          nr: catalogEntry.getMetadata().find(catalogEntry.getResourceURI(), 'dcat:dataset').length,
        });
      }).then(() => {
        catalogs.sort((c1, c2) => (c1.nr < c2.nr ? 1 : -1));
        this.render(catalogs);
      });
    },

    render(catalogs) {
      domAttr.set(this.domNode, 'innerHTML', '');
      catalogs.forEach((catalog) => {
        const p = lang.clone(params);
        p.context = catalog.entry.getContext().getId();
        domConstruct.create('a', {
          class: 'list-group-item',
          href: '', // TODO
          innerHTML: `<span class='badge'>${catalog.nr}</span>${catalog.label}`,
        }, this.domNode);
      });
    },
  }));
