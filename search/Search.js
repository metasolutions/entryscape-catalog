define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-class',
  'dojo/query',
  'dojo/aspect',
  'config',
  'entryscape-commons/defaults',
  'entryscape-commons/placeholder/Placeholder',
  'rdforms/view/Presenter',
  './DatasetSearch',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'jquery',
  'dojo/text!./SearchTemplate.html',
], (declare, domConstruct, domAttr, domStyle, domClass, query, aspect, config, defaults,
    Placeholder, Presenter, DatasetSearch, _WidgetBase, _TemplatedMixin, jquery, template) =>

  declare([_WidgetBase, _TemplatedMixin], {
    bid: 'escaSearch',
    templateString: template,
    datasetSearchView: 'catalog__dataset__search',

    __catalogList: null,
    __main: null,
    __label: null,
    __catalogMetadata: null,
    __back: null,

    postCreate() {
      this.inherited(arguments);
      this.list = new DatasetSearch({}, domConstruct.create('div', null, this.__main));
      this.list.render();
      this.placeholder = new Placeholder({}, domConstruct.create('div', null, this.__placeholder));
      this.placeholder.show();
    },

    show() {
      this.inherited(arguments);
      const rdfutils = defaults.get('rdfutils');
      const catalogs = [];
      let solrQuery = defaults.get('entrystore').newSolrQuery().rdfType('dcat:Catalog');
      if (config.catalog && config.catalog.excludeEmptyCatalogsInSearch) {
        solrQuery = solrQuery.uriProperty('dcat:dataset', '*');
      }
      solrQuery.forEach((catalogEntry) => {
        catalogs.push({
          entry: catalogEntry,
          label: rdfutils.getLabel(catalogEntry),
          nr: catalogEntry.getMetadata().find(catalogEntry.getResourceURI(), 'dcat:dataset').length,
        });
      }).then(() => {
        catalogs.sort((c1, c2) => (c1.nr < c2.nr ? 1 : -1));
        this.render(catalogs);
      });
    },
    render(catalogs) {
      this.catalogs = catalogs;
      const context = defaults.get('context');
      domAttr.set(this.__catalogList, 'innerHTML', '');

      domClass.toggle(this.domNode, `${this.bid}--catalogs`, catalogs.length !== 0);
      let catalog;
      if (catalogs.length === 1) {
        catalog = catalogs[0];
      } else if (context) {
        if (catalogs.length !== 0) {
          catalogs.some((c) => {
            if (c.entry.getContext() === context) {
              catalog = c;
              return true;
            }
            return false;
          });
        }
      }

      const site = defaults.get('siteManager');
      const params = site.getUpcomingOrCurrentParams();
      const view = site.getUpcomingOrCurrentView();
      if (catalog) {
        this.catalogEntry = catalog.entry;
        domClass.remove(this.domNode, `${this.bid}--list`);
        domAttr.set(this.__label, 'innerHTML', catalog.label);
        this.showCatalogDetails();
        const p = Object.assign({}, params);
        delete p.context;
        if (catalogs.length <= 1) {
          domClass.add(this.domNode, `${this.bid}--noListAvailable`);
        } else {
          domClass.remove(this.domNode, `${this.bid}--noListAvailable`);
          domAttr.set(this.__back, 'href', site.getViewPath(view, p));
        }
        domStyle.set(this.__catalogControls, 'display', '');
      } else {
        domClass.add(this.domNode, `${this.bid}--list`);
        catalogs.forEach((catal) => {
          const p = Object.assign({}, params);
          p.context = catal.entry.getContext().getId();
          domConstruct.create('a', {
            class: 'list-group-item list-group-item-action',
            href: site.getViewPath(this.datasetSearchView, p),
            innerHTML: `<span class='badge'>${catal.nr}</span>${catal.label}`,
          }, this.__catalogList);
        });

        domStyle.set(this.__catalogControls, 'display', 'none');
      }
      this.list.getView().action_refresh();
    },
    showCatalogDetails() {
      if (!this.catalogPresenter) {
        this.catalogPresenter = new Presenter({
          filterPredicates: { 'http://purl.org/dc/terms/title': true },
        }, domConstruct.create('div', null, this.__catalogMetadata));
      }
      const catalogTemplate = defaults.get('itemstore').getItem(config.catalog.catalogTemplateId);
      const resourceURI = this.catalogEntry.getResourceURI();
      const md = this.catalogEntry.getMetadata();
      this.catalogPresenter.show({ resource: resourceURI, graph: md, template: catalogTemplate });
    },
  }));
