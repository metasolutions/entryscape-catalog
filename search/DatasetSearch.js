define([
  'dojo/_base/declare',
  'config',
  'entryscape-commons/defaults',
  'entryscape-catalog/public/DatasetDialog',
  'entryscape-commons/list/common/BaseList',
  'entryscape-catalog/utils/ListView',
  'i18n!nls/escoList',
  'i18n!nls/escaDataset',
], (declare, config, defaults, DatasetDialog, BaseList, ListView) => {
  const ns = defaults.get('namespaces');

  return declare([BaseList], {
    includeCreateButton: false,
    includeInfoButton: true,
    includeEditButton: false,
    includeRemoveButton: false,
    includeHead: true,
    includeResultSize: false,
    searchVisibleFromStart: true,
    nlsBundles: ['escoList', 'escaDataset'],
    entryType: ns.expand('dcat:Dataset'),
    listViewClass: ListView,
    class: 'datasets',
    rowClickDialog: 'info',
    nlsEmptyListWarningKey: 'emptyListWarningForSearch',
    postCreate() {
      this.inherited('postCreate', arguments);
      this.registerDialog('info', DatasetDialog);
      this.listView.includeResultSize = !!this.includeResultSize; // make this boolean
    },
    showStopSign() {
      return false;
    },
    installButtonOrNot() {
      return true;
    },
    getIconClass() {
      return 'search';
    },
    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem(config.catalog.datasetTemplateId);
      }
      return this.template;
    },
    getSearchObject() {
      const so = defaults.get('entrystore').newSolrQuery().rdfType(this.entryType);
      const context = defaults.get('context');
      if (context) {
        so.context(context);
      }
      return so;
    },
  });
});
