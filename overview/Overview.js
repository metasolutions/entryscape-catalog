define([
  'jquery',
  'dojo/_base/lang',
  'dojo/_base/declare',
  'dojo/promise/all',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'store/types',
  'config',
  'di18n/localize',
  'entryscape-commons/overview/components/Overview',
  'entryscape-commons/defaults',
  'entryscape-commons/util/dateUtil',
  'mithril',
  'i18n!nls/escaOverview',
], (jquery, lang, declare, all, domAttr, domStyle, _WidgetBase, _TemplatedMixin,
    _WidgetsInTemplateMixin, NLSMixin, types, config, localize, Overview, defaults,
    dateUtil) =>

  declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    templateString: '<div class="catalogOverview escoList"></div>',
    nlsBundles: ['escaOverview'],
    getEntityNameFromURI(entityURI) {
      const es = defaults.get('entrystore');
      if (entityURI.indexOf(es.getBaseURI()) === 0) {
        return entityURI.substr(es.getResourceURI('entitytypes', '').length);
      }
      return entityURI;
    },
    show() {
      this.data = {};
      let catalogEntry;
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      const rdfutils = defaults.get('rdfutils');
      const spa = defaults.get('siteManager');
      const viewParams = spa.getUpcomingOrCurrentParams();
      const context = defaults.get('context');

      const querySListMap = new Map();
      const queryPromises = [];

      queryPromises.push(context.getEntry().then((entry) => {
        catalogEntry = entry;
      }));

      const rdfTypesObject = {
        dataset: 'dcat:Dataset',
        candidate: 'esterms:CandidateDataset',
        showcase: 'esterms:Result',
        contact: ['vcard:Kind', 'vcard:Individual', 'vcard:Organization'],
        publisher: ['foaf:Agent', 'foaf:Person', 'foaf:Organization'],
        idea: 'esterms:Idea',
      };
      Object.keys(rdfTypesObject).forEach((rdfType) => {
        const searchList =
          es.newSolrQuery().context(context).rdfType(rdfTypesObject[rdfType]).list();

        querySListMap.set(rdfType, searchList);
        queryPromises.push(searchList.getEntries(0));
      });

      all([this.localeReady, ...queryPromises]).then(() => {
        const modificationDate = catalogEntry.getEntryInfo().getModificationDate();
        const creationDate = catalogEntry.getEntryInfo().getCreationDate();
        const modificationDateFormats = dateUtil.getMultipleDateFormats(modificationDate);
        const creationDateFormats = dateUtil.getMultipleDateFormats(creationDate);

        // basic info
        this.data.description = defaults.get('localize')(rdfutils.getDescription(catalogEntry));
        this.data.title = defaults.get('localize')(rdfutils.getLabel(catalogEntry));

        const isNLSBundleReady = this.nlsBundles[0] in this.NLSBundles;
        let b;
        if (isNLSBundleReady) {
          b = this.NLSBundles[this.nlsBundles[0]];
        }

        // box list
        this.data.bList = [];
        querySListMap.forEach((searchList, rdfType) => {
          this.data.bList.push({
            key: rdfType,
            label: isNLSBundleReady ? localize(b, `${rdfType}Label`) : `${rdfType}Label`,
            value: searchList.getSize(),
            link: spa.getViewPath(`catalog__${rdfType}s`, viewParams),
          });
        });

        // stats list
        this.data.sList = [
          {
            key: 'update',
            label: isNLSBundleReady ? localize(b, 'lastUpdatedLabel') : 'lastUpdatedLabel',
            value: modificationDateFormats.short,
          },
          {
            key: 'create',
            label: isNLSBundleReady ? localize(b, 'createdLabel') : 'createdLabel',
            value: creationDateFormats.short,
          },
        ];
        m.render(jquery('.catalogOverview.escoList')[0], m(Overview, { data: this.data }));
      });
    },
  }));
