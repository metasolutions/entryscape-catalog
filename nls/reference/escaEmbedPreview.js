/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */
define({
  root: {
    embedPreviewHeader: "Preview of embed code",
  },
  sv: true,
  da: true,
  de: true,
});
