/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */
define({
  root: {
    datasetBelongsToCatalog: "This dataset belongs to catalog",
    accessDistributionsOfDataset: "You can access the dataset via its distributions",
    datasetDetails: "Dataset details",
    resultsOfDataset: "Showcases in which the dataset has been used",
  },
  sv: true,
  da: true,
  de: true,
});
