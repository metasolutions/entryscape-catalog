/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */
define({
  root: {
    manageFilesHeader: "Manage files",
    manageFilesFooterButton: "Done",
    reActivateAPI: "An API is already activated, do you want to refresh it?",
  },
  sv: true,
  da: true,
  de: true,
});
