/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */
define({
  root: {
    lastUpdatedLabel: "updated",
    createdLabel: "created",
    datasetLabel: "Datasets",
    candidateLabel: "Candidates",
    showcaseLabel: "Showcases",
    publisherLabel: "Publishers",
    contactLabel: "Contacts",
    ideaLabel: "Ideas",
  },
  sv: true,
  da: true,
  de: true,
});
