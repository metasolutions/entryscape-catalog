/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */
define({
  root: {
    showIdeasDialogHeader: "Ideas for dataset \"$1\"",
    emptyListWarning: "There are no ideas for this dataset.",
  },
  sv: true,
  da: true,
  de: true,
});
