/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */
define({
  root: {
    showResultsDialogHeader: "Showcases for dataset \"$1\"",
    emptyListWarning: "There are no showcases for this dataset.",
  },
  sv: true,
  da: true,
  de: true,
});
