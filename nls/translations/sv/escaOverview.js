define({
  "lastUpdatedLabel": "uppdaterad",
  "createdLabel": "skapad",
  "datasetLabel": "Datamängder",
  "candidateLabel": "Kandidater",
  "showcaseLabel": "Showcases",
  "publisherLabel": "Organisationer",
  "contactLabel": "Kontakter",
  "ideaLabel": "Idéer"
});