define([
  'dojo/_base/declare',
  'entryscape-commons/defaults',
  'entryscape-commons/list/EntryRow',
  'entryscape-commons/comments/comments',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/text!./CandidateRowTemplate.html',
  'dojo/number',
  'config',
  'di18n/localize',
], (declare, defaults, EntryRow, comments, domAttr, domClass, domStyle, template, number, config,
    localize) =>

  declare([EntryRow], {
    bid: 'escaCandidateRow',
    templateString: template,
    includeInfoButton: false,
    includeEditButton: true,
    includeRemoveButton: true,
    showCol1: true,

    postCreate() {
      this.inherited('postCreate', arguments);
      this.candidateEntry = this.entry;
      this.renderCol4();
    },
    updateLocaleStrings() {
      this.inherited('updateLocaleStrings', arguments);
      this.maybeUpdate();
    },
    render() {
      this.inherited(arguments);
      this.maybeUpdate();
    },
    renderCol1() {
      if (config.catalog && config.catalog.checklist) {
        const checklistSteps = config.catalog.checklist;
        const completedChecklistSteps = [];
        const mandatoryChecklistSteps = [];
        const infoEntryGraph = this.candidateEntry.getEntryInfo().getGraph();
        const tasks = infoEntryGraph.find(this.candidateEntry.getResourceURI(), 'http://entrystore.org/terms/progress');
        tasks.forEach((task) => {
          completedChecklistSteps.push(task.getObject().value);
        });
        this.noOfTasksCompleted = completedChecklistSteps.length;
        this.noOfCheckListSteps = checklistSteps.length;
        let progress = number.round((this.noOfTasksCompleted * 100) / checklistSteps.length);
        if (progress > 0) {
          progress -= 2;
        }

        checklistSteps.forEach((checklistStep) => {
          if (checklistStep.mandatory) {
            mandatoryChecklistSteps.push({
              name: checklistStep.name,
            });
          }
        });
        let isMandatoryCheckslistCompleted = true;
        let mandatory = mandatoryChecklistSteps.length;
        mandatoryChecklistSteps.forEach((mandatoryChecklistStep) => {
          if (completedChecklistSteps.indexOf(mandatoryChecklistStep.name) === -1) {
            isMandatoryCheckslistCompleted = false;
            mandatory -= 1;
          }
        });
        this.noOfMandatoryCompleted = mandatory;
        this.noOfMandatory = mandatoryChecklistSteps.length;
        if (isMandatoryCheckslistCompleted) {
          domClass.remove(this.__progressBar);
          domClass.add(this.__progressBar, 'escaCandidateRow__progressBar escaCandidateRow__progressBar--ready');
        } else {
          domClass.remove(this.__progressBar);
          domClass.add(this.__progressBar, 'escaCandidateRow__progressBar  escaCandidateRow__progressBar--incomplete');
        }
        domStyle.set(this.__progressBar, 'border-left-width', `${progress}px`);
      }
    },
    renderCol4() {
      // badgeNode
      comments.getNrOfComments(this.entry).then((nr) => {
        this.noOfComments = nr;
        if (nr > 0) {
          domStyle.set(this.badgeNode, 'display', '');
          domAttr.set(this.badgeNode, 'innerHTML', nr);
          this.maybeUpdate();
        }
      });
    },
    decreaseReplyCount() {
      this.noOfComments -= 1;
      this.renderCommentCount();
    },
    renderCommentCount() {
      // badgeNode
      if (this.noOfComments > 0) {
        domStyle.set(this.badgeNode, 'display', '');
        domAttr.set(this.badgeNode, 'innerHTML', this.noOfComments);
      } else {
        domStyle.set(this.badgeNode, 'display', 'none');
      }
      this.maybeUpdate();
    },
    maybeUpdate() {
      if (this.nlsSpecificBundle) {
        if (this.noOfComments > 0) {
          const tStr = localize(this.nlsSpecificBundle, 'commentTitle', this.noOfComments);
          domAttr.set(this.badgeNode, 'title', tStr);
        }
        domAttr.set(this.__progressBarOuter, 'title', localize(this.nlsSpecificBundle, 'progressBarTitle', {
          checked: this.noOfTasksCompleted,
          total: this.noOfCheckListSteps,
          mandatory: this.noOfMandatoryCompleted,
          totalMandatory: this.noOfMandatory,
        }));
      }
    },
    openCommentDialog(ev) {
      this.list.openDialog('comment', { row: this });
      ev.stopPropagation();
    },
    action_remove() {
      const dialogs = defaults.get('dialogs');
      const self = this;
      let confirmMessage;
      if (this.noOfComments > 0) {
        confirmMessage = localize(this.nlsSpecificBundle, 'removeCdatasetAndComments', this.noOfComments);
      } else {
        confirmMessage = this.nlsSpecificBundle.removeCdataset;
      }

      dialogs.confirm(confirmMessage, null, null, (confirm) => {
        if (!confirm) {
          return null;
        }
        return comments.getReplyList(this.entry)
          .forEach(comment => comment.del()).then(() => self.entry.del().then(() => {
            self.list.getView().removeRow(self);
            self.destroy();
          }), () => {
            self.renderCommentCount();
            dialogs.acknowledge(self.nlsSpecificBundle.failedToRemoveCDatasetComments);
          });
      });
    },
  }));
