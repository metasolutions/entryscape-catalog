define([
  'dojo/_base/declare',
  '../defaults',
  'entryscape-commons/list/common/ETBaseList',
  './CandidateRow',
  '../utils/ListView',
  './UpgradeDialog',
  'entryscape-commons/rdforms/RDFormsEditDialog',
  'config',
  'entryscape-commons/progress/ProgressDialog',
  'entryscape-commons/comments/CommentDialog',
  'di18n/localize',
  'i18n!nls/escoList',
  'i18n!nls/escaCandidates',
], (declare, defaults, ETBaseList, CandidateRow, ListView, UpgradeDialog, RDFormsEditDialog,
    config, ProgressDialog, CommentDialog, localize) => {
  const ns = defaults.get('namespaces');

  const CreateDialog = declare(RDFormsEditDialog, {
    maxWidth: 800,
    explicitNLS: true,
    constructor(params) {
      this.list = params.list;
    },
    open() {
      this.list.getView().clearSearch();
      this.title = this.list.nlsSpecificBundle.createCandidateDatasetHeader;
      this.doneLabel = this.list.nlsSpecificBundle.createCandidateDatasetButton;
      this.updateTitleAndButton();
      const nds = defaults.get('createEntry')(null, 'dcat:Dataset');
      this._newCandidate = nds;
      defaults.get('getGroupWithHomeContext')(nds.getContext())
        .then((groupEntry) => {
          const ei = nds.getEntryInfo();
          const acl = ei.getACL(true);
          acl.admin.push(groupEntry.getId());
          ei.setACL(acl);
        });

      nds.getMetadata().add(nds.getResourceURI(), 'rdf:type', 'esterms:CandidateDataset');
      this.show(nds.getResourceURI(), nds.getMetadata(),
        this.list.getTemplate(), this.list.getTemplateLevel(nds));
    },
    doneAction(graph) {
      return this._newCandidate.setMetadata(graph).commit()
        .then((newEntry) => {
          this.list.getView().addRowForEntry(newEntry);
          return newEntry.refresh();
        });
    },
  });

  const CommentDialog2 = declare([CommentDialog], {
    maxWidth: 800,
    title: 'temporary', // to avoid exception
    open(params) {
      this.inherited(arguments);
      const name = defaults.get('rdfutils').getLabel(params.row.entry);
      this.title = localize(this.list.nlsSpecificBundle, 'commentHeader', { name });
      this.footerButtonLabel = this.list.nlsSpecificBundle.commentFooterButton;
      this.localeChange();
    },
  });

  return declare([ETBaseList], {
    includeCreateButton: true,
    includeInfoButton: false,
    includeEditButton: true,
    includeRemoveButton: true,
    nlsBundles: ['escoList', 'escaCandidates'],
    entitytype: 'candidate',
    entryType: ns.expand('esterms:CandidateDataset'),
    nlsCreateEntryLabel: 'createCandidate',
    nlsCreateEntryTitle: 'createCandidatePopoverTitle',
    nlsCreateEntryMessage: 'createCandidatePopoverMessage',
    class: 'candidatedataset',
    listViewClass: ListView,
    rowClass: CandidateRow,
    searchVisibleFromStart: false,
    // Versions explicitly excluded since the support for checklist is not in metadata
    rowActionNames: ['edit', 'upgrade', 'progress', 'comment', 'remove'],
    rowClickDialog: 'progress',

    postCreate() {
      this.registerDialog('progress', ProgressDialog);
      this.registerDialog('upgrade', UpgradeDialog);
      this.registerDialog('comment', CommentDialog2);
      this.registerRowAction({
        name: 'progress',
        button: 'default',
        icon: 'check-square',
        iconType: 'fa',
        nlsKey: 'progressMenu',
        nlsKeyTitle: 'progressTitle',
      });
      this.registerRowAction({
        name: 'upgrade',
        button: 'default',
        icon: 'level-up',
        iconType: 'fa',
        nlsKey: 'upgrade',
        nlsKeyTitle: 'upgradeTitle',
      });
      this.registerRowAction({
        name: 'comment',
        button: 'default',
        icon: 'comment',
        iconType: 'fa',
        nlsKey: 'commentMenu',
        nlsKeyTitle: 'commentMenuTitle',
      });
      this.inherited('postCreate', arguments);
      this.registerDialog('create', CreateDialog);
      this.dialogs.create.levels.setIncludeLevel('optional');
    },

    show() {
      const self = this;
      const esu = defaults.get('entrystoreutil');
      esu.preloadEntries(ns.expand('esterms:CandidateDataset'), null).then(self.render());
    },
    getSearchObject() {
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      return es.newSolrQuery().rdfType(ns.expand('esterms:CandidateDataset'))
        .context(defaults.get('context'));
    },
    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem(config.catalog.datasetCandidateTemplateId);
      }
      return this.template;
    },
    getTemplateLevel() {
      return 'recommended';
    },
  });
});
