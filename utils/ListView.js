define([
  'dojo/dom-attr',
  'dojo/_base/declare',
  'dojo/Deferred',
  '../defaults',
  'di18n/localize',
  'entryscape-commons/list/ListView',
], (domAttr, declare, Deferred, defaults, localize, ListView) =>

  declare([ListView], {
    constructor() {
      this.catalogPromise = new Deferred();
    },
    updateHeader() {
      const context = defaults.get('context');
      if (context) {
        if (this.catalogPromise.isFulfilled()) {
          this.catalogPromise = new Deferred();
        }
        defaults.get('entrystoreutil')
          .getEntryByType('dcat:Catalog', context)
          .then((dcat) => {
            const rdfutils = defaults.get('rdfutils');
            const catalogName = dcat === null ? '?' : rdfutils.getLabel(dcat) || '-';
            const lhk = this.nlsListHeaderKey;
            const lhtk = this.nlsListHeaderTitleKey;
            const listheader =
              localize(this.nlsSpecificBundle, lhk, { 1: catalogName })
              || localize(this.nlsGenericBundle, lhk, { 1: catalogName })
              || '';

            domAttr.set(this.listHeader, 'innerHTML', listheader);
            domAttr.set(this.listHeader, 'title', this.nlsSpecificBundle[lhtk] || this.nlsGenericBundle[lhtk] || '');
            this.catalogPromise.resolve(dcat);
          }, (err) => {
            this.catalogPromise.reject(err);
          });
      }
    },
  }));
