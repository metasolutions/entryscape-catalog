define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-style',
  'entryscape-commons/defaults',
  'entryscape-commons/list/EntryRow',
  'di18n/localize',
  'dojo/text!./RowTemplate.html',
], (declare, lang, domStyle, defaults, EntryRow, localize, template) => {
  const strEndsWith = (str, suffix) => str.match(`${suffix}$`) === suffix;

  return declare([EntryRow], {
    templateString: template,
    showCol1: true,

    postCreate() {
      this.inherited('postCreate', arguments);
      if (this.list.isEntryAgent(this.entry)) {
        domStyle.set(this.foafNode, 'display', '');
        domStyle.set(this.cpNode, 'display', 'none');
      } else {
        domStyle.set(this.foafNode, 'display', 'none');
        domStyle.set(this.cpNode, 'display', '');
      }
    },

    updateLocaleStrings() {
      this.inherited('updateLocaleStrings', arguments);
    },

    action_remove() {
      const bundle = this.nlsSpecificBundle;
      const dialogs = defaults.get('dialogs');
      const ns = defaults.get('namespaces');
      this.getReferences().then((refs) => {
        if (refs.length === 0) {
          dialogs.confirm(bundle.removeResponsible, null, null, (confirm) => {
            if (!confirm) {
              return;
            }
            this.entry.del()
              .then(lang.hitch(this, this.destroy), () =>
                dialogs.acknowledge('Failed to remove responsible'));
          });
        } else {
          const lbls = refs.map((ref, idx) => {
            if (idx === 5) {
              return '<li> ...</li>';
            } else if (idx > 5) {
              return '';
            }
            const t = ref.getMetadata().findFirstValue(ref.getResourceURI(), ns.expand('rdf:type')) || '';
            const rdfutils = defaults.get('rdfutils');
            let l = rdfutils.getLabel(ref);
            if (l == null) {
              l = localize(bundle, 'entryWithId', `'${ref.getId()}'`);
            } else {
              l = `'${l}'`;
            }
            if (strEndsWith(t, 'Dataset')) {
              return `<li>${localize(bundle, 'datasetAsRemoveItem', { 1: l })}</li>`;
            } else if (strEndsWith(t, 'Catalog')) {
              return `<li>${localize(bundle, 'catalogAsRemoveItem', { 1: l })}</li>`;
            } else if (strEndsWith(t, 'Distribution')) {
              return `<li>${localize(bundle, 'distributionAsRemoveItem', { 1: l })}</li>`;
            }
            return `<li>${localize(bundle, 'otherAsRemoveItem', { 1: l })}</li>`;
          });
          dialogs.acknowledge(localize(bundle, 'responsibleUnableToRemove', { 1: `<ul class='inUseBy'>${lbls.join('')}</ul>` }));
        }
      });
    },
    getReferences() {
      return defaults.get('entrystore').newSolrQuery()
        .objectUri(this.entry.getResourceURI()).getEntries();
    },
  });
});
