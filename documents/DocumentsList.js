define([
  'dojo/_base/declare',
  '../defaults',
  'entryscape-workbench/bench/List',
  'entryscape-commons/create/typeIndex',
  'i18n!nls/escoList',
  'i18n!nls/escaDocuments',
], (declare, defaults, List, typeIndex) => {

  return declare([List], {
    nlsBundles: ['escoList', 'escaDocuments'],
    nlsRemoveEntryConfirm: 'confirmDocumentRemove',
    includeMassOperations: false,
    rowActionNames: ['edit', 'replace', 'download', 'remove'],

    constructor() {
      this.benchTypeConf = typeIndex.getConfByName('datasetDocument');
      this.bench = {
        updateBadge: () => {},
      };
      this.mode = 'edit';
    },
  });
});
