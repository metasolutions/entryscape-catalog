module.exports = (grunt) => {
  grunt.task.loadTasks('node_modules/entryscape-js/tasks');

  grunt.config.merge({
    poeditor: {
      projectids: ['94869'],
    },
    nls: {
      langs: ['en', 'sv', 'da', 'de'],
      depRepositories: ['entryscape-commons', 'entryscape-admin', 'entryscape-workbench'],
    },
    update: {
      libs: [
        'di18n',
        'spa',
        'rdfjson',
        'rdforms',
        'store',
        'entryscape-commons',
        'entryscape-admin',
      ],
    },
  });

  grunt.loadNpmTasks('grunt-available-tasks');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
};
