define([
  'dojo/_base/declare',
  'dojo/on',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/promise/all',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'di18n/NLSMixin',
  'entryscape-commons/defaults',
  'rdforms/view/Presenter', // In template
  'entryscape-commons/rdforms/RDFormsPresentDialog', // In template
  'dojo/text!./APIInfoTemplate.html',
  'i18n!nls/escaFiles',
], (declare, on, domAttr, domStyle, all, _WidgetBase, _TemplatedMixin, NLSMixin, defaults,
    Presenter, RDFormsPresentDialog, template) => {
  const ns = defaults.get('namespaces');
  const resultColumnName = ns.expand('store:pipelineResultColumnName');

  return declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    templateString: template,
    nlsBundles: ['escaFiles'],

    postCreate() {
      this.inherited('postCreate', arguments);
    },
    hide() {
      domStyle.set(this.domNode, 'display', 'none');
    },
    show(entry) {
      const stmts = entry.getCachedExternalMetadata().find(null, resultColumnName);
      const cols = stmts.map(stmt => stmt.getValue());
      if (cols.length > 0) {
        domStyle.set(this.domNode, 'display', '');
        domAttr.set(this.apiColumns, 'innerHTML', cols.join(', '));
        const exampleURL = `${entry.getResourceURI()}?${cols[0]}=some_string_pattern`;
        domAttr.set(this.searchExampleLink, 'innerHTML', exampleURL);
        domAttr.set(this.searchExampleLink, 'href', exampleURL);
      }
    },
  });
});
