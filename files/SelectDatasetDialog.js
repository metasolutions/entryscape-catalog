define([
  'dojo/_base/declare',
  'dojo/dom-class',
  '../defaults',
  'di18n/NLSMixin',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/list/common/BaseList',
  'i18n!nls/escoList',
  'i18n!nls/escaFiles',
], (declare, domClass, defaults, NLSMixin, TitleDialog, BaseList) => {
  const ns = defaults.get('namespaces');

  const SelectList = declare([BaseList], {
    includeCreateButton: false,
    includeInfoButton: true,
    includeEditButton: false,
    includeRemoveButton: false,
    includeHeadBlock: false,
    listInDialog: true,
    searchVisibleFromStart: true,
    nlsBundles: ['escoList', 'escaFiles'],
    entryType: ns.expand('dcat:Dataset'),
    rowClickDialog: 'select',

    postCreate() {
      this.inherited('postCreate', arguments);
      domClass.remove(this.getView().domNode, 'container');
    },
    showStopSign() {
      return false;
    },
    installButtonOrNot() {
      return true;
    },

    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem('dcat:OnlyDataset');
      }
      return this.template;
    },

    openDialog(dialogName, params) {
      if (dialogName === 'select') {
        this.detailsDialog.connectToDataset(params.row.entry);
        this.selectionDialog.hide();
      } else {
        this.inherited(arguments);
      }
    },

    getSearchObject() {
      const context = defaults.get('context');
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      return es.newSolrQuery().rdfType(this.entryType).context(context.getResourceURI());
    },
  });

  return declare([TitleDialog, NLSMixin.Dijit], {
    nlsHeaderTitle: 'connectToExistingDataset',
    includeFooter: false,
    nlsBundles: ['escaFiles'],

    postCreate() {
      this.inherited(arguments);
      this.selectedList = new SelectList({ selectionDialog: this }, this.containerNode);
    },
    show(detailsDialog) {
      this.selectedList.detailsDialog = detailsDialog;
      this.selectedList.render();
      this.inherited(arguments);
    },
    localeChange() {
      this.updateLocaleStrings(this.NLSBundles.escaFiles);
    },
  });
});
