define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/string',
  'dojo/json',
  'dojo/Deferred',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/promise/all',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  '../defaults',
  'di18n/NLSMixin',
  'store/utils',
  './SelectDatasetDialog', // In template
  'entryscape-commons/list/common/ListDialogMixin',
  'entryscape-commons/rdforms/RDFormsEditDialog',
  'entryscape-catalog/files/APIInfo', // In template
  'entryscape-commons/dialog/HeaderDialog', // In template
  'dojo/text!./DetailsDialogTemplate.html',
  'i18n!nls/escaFiles',
], (declare, lang, array, string, json, Deferred, domAttr, domStyle, domConstruct, all,
    _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, defaults, NLSMixin, utils,
    SelectDatasetDialog, ListDialogMixin, RDFormsEditDialog, APIInfo, HeaderDialog, template) => {
  const ns = defaults.get('namespaces');
  const resultStatus = ns.expand('store:pipelineResultStatus');
  const store = defaults.get('entrystore');
  const resultColumnName = ns.expand('store:pipelineResultColumnName');

  const createFileDistribution = (fileEntry) => {
    const context = defaults.get('context');
    const nds = context.newNamedEntry();
    const md = nds.getMetadata();
    md.add(nds.getResourceURI(), ns.expand('rdf:type'), ns.expand('dcat:Distribution'));
    md.add(nds.getResourceURI(), ns.expand('dcat:accessURL'), fileEntry.getResourceURI());
    md.add(nds.getResourceURI(), ns.expand('dcat:downloadURL'), fileEntry.getResourceURI());
    const format = fileEntry.getEntryInfo().getFormat();
    if (typeof format !== 'undefined') {
      md.add(nds.getResourceURI(), ns.expand('dcterms:format'), { type: 'literal', value: format });
    }
    return nds.commit();
  };

  /**
   *
   * @param etlEntry aka pipelineResult, points to the fileEntry (csv file) via pipelineData.
   * @param fileEntry
   * @returns {Promise}
   */
  const createAPIDistribution = (etlEntry, fileEntry) => {
    const nds = fileEntry.getContext().newNamedEntry();
    const md = nds.getMetadata();
    md.add(nds.getResourceURI(), ns.expand('rdf:type'), ns.expand('dcat:Distribution'));
    md.add(nds.getResourceURI(), ns.expand('dcat:accessURL'), etlEntry.getResourceURI());
    md.add(nds.getResourceURI(), ns.expand('dcterms:source'), fileEntry.getResourceURI());
    md.add(nds.getResourceURI(), ns.expand('dcterms:format'), {
      type: 'literal',
      value: 'application/json',
    });
    return nds.commit();
  };

  const addDistributionsToDatasetNoCommit = (dataset, distributions) => {
    const md = dataset.getMetadata();
    array.forEach(distributions, (dist) => {
      md.add(dataset.getResourceURI(),
        ns.expand('dcat:distribution'), dist.getResourceURI());
    }, this);
  };

  const CreateDialog = declare(RDFormsEditDialog, {
    explicitNLS: true,
    maxWidth: 800,
    open(callback) {
      const onlyDatasetTemplate = defaults.get('itemstore').getItem('dcat:OnlyDataset');
      this.fileEntry = this.details.entry;
      this.callback = callback;
      this.doneLabel = this.list.nlsSpecificBundle.createDataset;
      this.title = this.list.nlsSpecificBundle.createDatasetHeader;
      this.updateTitleAndButton();
      const context = defaults.get('context');
      this._newDataset = context.newNamedEntry();
      const nds = this._newDataset;
      nds.getMetadata().add(
        nds.getResourceURI(), ns.expand('rdf:type'), ns.expand('dcat:Dataset'));
      this.show(nds.getResourceURI(), nds.getMetadata(), onlyDatasetTemplate);
    },
    doneAction(graph) {
      this._newDataset.setMetadata(graph);
      return this.createDistributions(this.fileEntry)
        .then(lang.hitch(this, this.createDataset))
        .then(lang.hitch(this, this.connectToCatalog))
        .then(lang.hitch(this, this.refreshAll));
    },
    createDistributions(fileEntry) {
      const promises = [createFileDistribution(fileEntry)];
      if (this.details.etlEntry) {
        promises.push(createAPIDistribution(this.details.etlEntry, fileEntry));
      }
      return all(promises);
    },
    createDataset(distributionEntries) {
      this.distributionEntries = distributionEntries;
      addDistributionsToDatasetNoCommit(this._newDataset, distributionEntries);
      return this._newDataset.commit();
    },
    connectToCatalog(newDatasetEntry) {
      this.datasetEntry = newDatasetEntry;
      return store.getEntry(store.getEntryURI(newDatasetEntry.getContext().getId(), 'dcat'))
        .then((catalog) => {
          this.catalogEntry = catalog;
          catalog.getMetadata().add(catalog.getResourceURI(),
            ns.expand('dcat:dataset'), newDatasetEntry.getResourceURI());
          return catalog.commitMetadata().then(() => {
            newDatasetEntry.setRefreshNeeded();
            return newDatasetEntry;
          });
        });
    },
    refreshAll() {
      const entries = [this.datasetEntry, this.fileEntry].concat(this.distributionEntries);
      return all(array.map(entries, (d) => {
        d.setRefreshNeeded();
        return d.refresh();
      })).then(() => {
        this.callback();
      });
    },
  });

  return declare([
    _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin.Dijit], {
      templateString: template,
      nlsBundles: ['escaFiles'],

      postCreate() {
        this.inherited('postCreate', arguments);
        this.createNewDatasetDialog = new CreateDialog({ details: this }, domConstruct.create('div', null, this.dialogsNode));
      },

      open(params) {
        this.inherited(arguments);
        this.entry = params.row.entry;
        this.detectDataset();
        this.detectAPI();
        domAttr.set(this.titleNode, 'innerHTML', defaults.get('rdfutils').getLabel(this.entry));
        this.dialog.show();
      },

      detectDataset() {
        const contextId = this.entry.getContext().getId();
        const dists = this.entry.getReferrers(ns.expand('dcat:downloadURL'));
        if (dists.length > 0) {
          this.fileDistURI = store.getEntryURI(contextId, store.getEntryId(dists[0]));
          store.getEntry(this.fileDistURI).then(lang.hitch(this, (distEntry) => {
            this.fileDistEntry = distEntry;
            const datasets = distEntry.getReferrers(ns.expand('dcat:distribution'));
            if (datasets.length > 0) {
              const datasetURI = store.getEntryURI(contextId, store.getEntryId(datasets[0]));
              return store.getEntry(datasetURI);
            }
            return null;
          })).then(lang.hitch(this, this.renderConnectedDataset));
        } else {
          this.renderConnectedDataset(null);
        }
      },

      renderConnectedDataset(datasetEntry) {
        this.datasetEntry = datasetEntry;
        if (datasetEntry != null) {
          domStyle.set(this.connectedDatasetBlock, 'display', '');
          domStyle.set(this.noConnectedDatasetBlock, 'display', 'none');
          const rdfutils = defaults.get('rdfutils');
          const label = rdfutils.getLabel(datasetEntry);
          domAttr.set(this.connectedDatasetLabel, 'innerHTML', label);// datasetConnected
        } else {
          domStyle.set(this.connectedDatasetBlock, 'display', 'none');
          domStyle.set(this.noConnectedDatasetBlock, 'display', '');
        }
      },

      createNewDataset() {
        this.createNewDatasetDialog.open(() => {
          this.detectDataset();
          this.detectAPIDist();
        });
      },

      openConnectToSeletedDatasetDialog() {
        this.datasetSelectDialog.show(this);
      },
      connectToDataset(datasetEntry) {
        const fileEntry = this.entry;
        const etlEntry = this.etlEntry;
        this.renderConnectedDataset(datasetEntry);
        const promises = [createFileDistribution(fileEntry)];
        if (etlEntry) {
          promises.push(createAPIDistribution(etlEntry, fileEntry));
        }
        all(promises).then((dists) => {
          addDistributionsToDatasetNoCommit(datasetEntry, dists);
          datasetEntry.commitMetadata().then(() => {
            const entries = [fileEntry].concat(dists);
            if (etlEntry) {
              entries.push(etlEntry);
            }
            return all(array.map(entries, (e) => {
              e.setRefreshNeeded();
              return e.refresh();
            }));
          });
        });
      },
      createDataset(distributionEntries) {
        this.distributionEntries = distributionEntries;
        addDistributionsToDatasetNoCommit(this._newDataset, distributionEntries);
        return this._newDataset.commit();
      },

      disconnectDataset() {
        const arrdefs = [];
        if (this.fileDistEntry) {
          this.datasetEntry.getMetadata().findAndRemove(this.datasetEntry.getResourceURI(),
            ns.expand('dcat:distribution'), {
              type: 'uri',
              value: this.fileDistEntry.getResourceURI(),
            });
          arrdefs.push(this.fileDistEntry.del());
        }

        if (this.etlDistEntry) {
          this.datasetEntry.getMetadata().findAndRemove(this.datasetEntry.getResourceURI(),
            ns.expand('dcat:distribution'), {
              type: 'uri',
              value: this.etlDistEntry.getResourceURI(),
            });
          arrdefs.push(this.etlDistEntry.del());
        }

        if (arrdefs.length > 0) {
          arrdefs.push(this.datasetEntry.commitMetadata());
        }
        all(arrdefs).then(() => {
          const arrDefs = [];
          if (this.etlDistEntry) {
            delete this.etlDistEntry;
            // Need to refresh more?
          }
          if (this.fileDistEntry) {
            delete this.fileDistEntry;
            this.entry.setRefreshNeeded();
            arrDefs.push(this.entry.refresh());
          }
          all(arrDefs).then(() => {
            this.maybeRemoveDataset();
          });
        });
      },

      maybeRemoveDataset() {
        const dialogs = defaults.get('dialogs');
        const stmts = this.datasetEntry.getMetadata().find(this.datasetEntry.getResourceURI(), ns.expand('dcat:distribution'));
        if (stmts.length === 0) {
          dialogs.confirm(
            this.NLSBundles.escaFiles.removeOrphanedDataset,
            this.NLSBundles.escaFiles.confirmRemoveOrphanedDataset,
            this.NLSBundles.escaFiles.rejectRemoveOrphanedDataset,
            lang.hitch(this, this.removeConnectedDataset));
        } else {
          this.detectDataset();
        }
      },

      removeConnectedDataset(confirmed) {
        if (confirmed) {
          const dsURI = this.datasetEntry.getResourceURI();
          const dcatURI = store.getEntryURI(this.datasetEntry.getContext().getId(), 'dcat');
          this.datasetEntry.del().then(() => {
            delete this.datasetEntry;
            return store.getEntry(dcatURI).then((catalog) => {
              catalog.getMetadata().findAndRemove(catalog.getResourceURI(),
                ns.expand('dcat:dataset'), { type: 'uri', value: dsURI });
              return catalog.commitMetadata();
            });
          }).then(() => this.detectDataset);
        } else {
          this.detectDataset();
        }
      },

      detectAPI() {
        const es = this.entry.getEntryStore();
        const etls = this.entry.getReferrers(ns.expand('store:pipelineData'));
        domStyle.set(this.apiActivateButton, 'display', 'none');
        domStyle.set(this.apiDeactivateButton, 'display', 'none');
        domStyle.set(this.apiRefreshButton, 'display', 'none');
        domStyle.set(this.apiStatus, 'color', 'black');
        domStyle.set(this.APIInfo, 'display', 'none');
        this.APIInfo.hide();
        if (etls.length === 0) {
          domStyle.set(this.apiActivateButton, 'display', '');
          domAttr.set(this.apiStatus, 'innerHTML',
            this.NLSBundles.escaFiles.apiNotConnected);
        } else {
          es.getEntry(etls[0])
            .then(lang.hitch(this, this.getAPIStatus))
            .then((status) => {
              domStyle.set(this.apiDeactivateButton, 'display', '');
              const statusMessageKey = `apiStatus_${status}`;
              domAttr.set(this.apiStatus, 'innerHTML',
                this.NLSBundles.escaFiles[statusMessageKey]);
              switch (status) {
                case 'error':
                  domStyle.set(this.apiStatus, 'color', 'red');
                  break;
                case 'available':
                  domStyle.set(this.apiStatus, 'color', 'green');
                  break;
                default:
                  domStyle.set(this.apiStatus, 'color', 'orange');
                  domStyle.set(this.apiRefreshButton, 'display', '');

              }
              this.APIInfo.show(this.etlEntry);
            });
          this.detectAPIDist();
        }
      },
      detectAPIDist() {
        const es = this.entry.getEntryStore();
        const dists = this.entry.getReferrers(ns.expand('dcterms:source'));
        if (dists.length !== 0) {
          const distEntryURI =
            es.getEntryURI(this.entry.getContext().getId(), es.getEntryId(dists[0]));
          es.getEntry(distEntryURI).then((e) => {
            this.etlDistEntry = e;
          });
        }
      },

      activateAPI() {
        const context = this.entry.getContext();
        const self = this;
        const f = () => {
          context.getEntryById('rowstorePipeline').then(
            pipeline => pipeline, () => {
              const pipProtEnt = context.newPipeline('rowstorePipeline');
              const pipRes = pipProtEnt.getResource();
              pipRes.addTransform(pipRes.transformTypes.ROWSTORE, {});
              return pipProtEnt.commit();
            })
            .then((pipeline) => {
              pipeline.getResource().then((pres) => {
                pres.execute(self.entry).then((result) => {
                  const obj = json.parse(result);
                  self.createDistributionForAPI(obj.result[0]).then(() => {
                    self.detectAPI();
                  }, () => {
                    self.entry.setRefreshNeeded();
                    self.entry.refresh().then(() => {
                      self.detectAPI();
                    });
                  });
                });
              });
            });
        };
        const format = this.entry.getEntryInfo().getFormat();
        if (format !== 'text/csv') {
          const dialogs = defaults.get('dialogs');
          dialogs.confirm(string.substitute(this.NLSBundles.escaFiles.onlyCSVSupported, { format: format || '-' }), this.NLSBundles.escaFiles.confirmAPIActivation, this.NLSBundles.escaFiles.abortAPIActivation).then(f);
        } else {
          f();
        }
      },

      createDistributionForAPI(pipelineResultEntryURI) {
        if (!pipelineResultEntryURI || !this.datasetEntry) {
          const d = new Deferred();
          d.reject(!pipelineResultEntryURI ? 'No API to create distribution for.' : 'No Dataset to create distribution in.');
          return d;
        }
        const fileEntry = this.entry;
        const datasetEntry = this.datasetEntry;
        return this.entry.getEntryStore().getEntry(pipelineResultEntryURI)
          .then(prEntry => createAPIDistribution(prEntry, fileEntry).then((distEntry) => {
            fileEntry.setRefreshNeeded();
            fileEntry.refresh();
            return utils.addRelation(datasetEntry, ns.expand('dcat:distribution'), distEntry);
          }));
      },

      removeDistributionForAPI() {
        delete this.etlDistEntry;
        const es = this.entry.getEntryStore();
        const contextId = this.entry.getContext().getId();
        const APIdists = this.entry.getReferrers(ns.expand('dcterms:source'));
        if (APIdists.length > 0 && this.datasetEntry) {
          const uri = es.getEntryURI(contextId, es.getEntryId(APIdists[0]));
          return es.getEntry(uri).then(dist => utils.remove(dist));
        }
        const d = new Deferred();
        d.resolve();
        return d;
      },

      deactivateAPI() {
        const self = this;
        const es = this.entry.getEntryStore();
        const contextId = this.entry.getContext().getId();
        const f = lang.hitch(this, () => {
          if (self.etlEntry) {
            const uri = `${es.getBaseURI() + contextId}/resource/${self.etlEntry.getId()}`;
            return es.getREST().del(`${uri}?proxy=true`)
              .then(() => self.etlEntry.del().then(() => {
                delete self.etlEntry;
                self.entry.setRefreshNeeded();
                return self.entry.refresh().then(() => {
                  self.detectAPI();
                });
              })); // TODO, handle if rowstore dataset already is removed
          }
          // TODO check if null is the right thing to return here
          return null;
        });
        this.removeDistributionForAPI().then(f, f);
      },

      refreshAPIStatus() {
        this.detectAPI();
      },

      getAPIStatus(etlEntry) {
        this.etlEntry = etlEntry;
        const extMD = etlEntry.getCachedExternalMetadata();
        let status = extMD.findFirstValue(null, resultStatus);
        if (status != null) {
          return status;
        }
        const es = etlEntry.getEntryStore();
        return es.loadViaProxy(etlEntry.getEntryInfo().getExternalMetadataURI()).then((data) => {
          switch (data.status) {
            case 0:
              return 'created';
            case 1:
              return 'accepted';
            case 2:
              return 'processing';
            case 3:
              status = 'available';
              break;
            case 4:
              status = 'error';
              break;
            default:
          }
          if (data.columnnames && data.columnnames.length > 0) {
            array.forEach(data.columnnames, (col) => {
              extMD.add(etlEntry.getResourceURI(), resultColumnName, { type: 'literal', value: col });
            });
          }
          extMD.add(etlEntry.getResourceURI(), resultStatus, { type: 'literal', value: status });
          etlEntry.commitCachedExternalMetadata();
          return status;
        });
      },
    });
});
