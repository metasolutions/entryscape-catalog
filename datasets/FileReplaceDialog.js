define([
  'dojo/_base/declare',
  'entryscape-commons/defaults',
  'store/utils',
  'entryscape-workbench/bench/ReplaceDialog',
  './GenerateAPI',
  'dojo/date/stamp',
  'i18n!nls/eswoReplaceDialog',
  'i18n!nls/escaManageFiles',
], (declare, defaults, utils, ReplaceDialog, GenerateAPI, stamp) => declare(ReplaceDialog, {
  nlsBundles: ['eswoReplaceDialog', 'escaManageFiles',
    'escaApiProgress', 'escaFiles'],
  open(params) {
    this.distributionEntry = params.distributionEntry;
    this.apiEntryURIs = params.apiEntryURIs;
    this.datasetEntry = params.datasetEntry;
    this.distributionRow = params.distributionRow;
    this.entry = params.entry;
    this.inherited(arguments);
  },
  isFileDistributionWithAPI() { // change conditions
    const es = defaults.get('entrystore');
    const baseURI = es.getBaseURI();
    return (this.entry.getResourceURI().indexOf(baseURI) > -1) &&
      (this.apiEntryURIs.indexOf(this.distributionEntry.getResourceURI()) > -1);
  },
  _getApiDistributionEntry() {
    const es = defaults.get('entrystore');
    const esu = defaults.get('entrystoreutil');
    const list = es.newSolrQuery()
      .rdfType('dcat:Distribution')
      .uriProperty('dcterms:source', this.distributionEntry.getResourceURI())
      .limit(1)
      .list();
    return list.getEntries().then(distEntries => esu.getEntryByResourceURI(
      distEntries[0].getResourceURI()));
  },
  footerButtonAction() {
    const inp = this.fileOrLink.getFileInputElement();
    const md = this.entry.getMetadata();
    md.findAndRemove(null, 'dcterms:title');
    md.addL(this.entry.getResourceURI(), 'dcterms:title', this.fileOrLink.getValue());
    return this.entry.commitMetadata().then(() =>
      this.entry.getResource().then(resource => resource.putFile(inp).then(() => {
        this.entry.setRefreshNeeded();
        return this.entry.refresh().then(() => {
          const format = this.entry.getEntryInfo().getFormat();
          const distMd = this.distributionEntry.getMetadata();
          const distResourceURI = this.distributionEntry.getResourceURI();
          distMd.findAndRemove(distResourceURI, 'dcterms:format');
          distMd.addL(distResourceURI, 'dcterms:format', format);
          distMd.findAndRemove(distResourceURI, 'dcterms:modified');
          distMd.addD(distResourceURI, 'dcterms:modified', stamp.toISOString(new Date()), 'xsd:date');
          return this.distributionEntry.commitMetadata().then(() => {
            this.distributionRow.renderMetadata();
            if (this.isFileDistributionWithAPI()) {
              const dialogs = defaults.get('dialogs');
              const confirmMessage = this.NLSBundle1.reActivateAPI;
              return dialogs.confirm(confirmMessage, null, null, (confirm) => {
                if (!confirm) {
                  this.dialog.hide();
                  return;
                }
                // this.apiProgressDialog.open({});
                this._getApiDistributionEntry().then((apiDistrEntry) => {
                  const generateAPI = new GenerateAPI();
                  generateAPI.show({
                    apiDistrEntry,
                    distributionEntry: this.distributionEntry,
                    datasetEntry: this.datasetEntry,
                    mode: 'edit',
                    escaApiProgress: this.NLSBundles.escaApiProgress,
                    escaFiles: this.NLSBundles.escaFiles,
                  });
                });
              });
            }
            return null;
          });
        });
      })));
  },
}));
