define([
  'dojo/_base/declare',
  'entryscape-commons/defaults',
  'entryscape-commons/dialog/TitleDialog', // In template
  'entryscape-commons/list/common/ListDialogMixin',
  'di18n/NLSMixin',
  'dojo/dom-construct',
  'entryscape-commons/list/common/BaseList',
  'config',
  'entryscape-commons/create/typeIndex',
  'di18n/localize',
  'i18n!nls/escaShowIdeas',
], (declare, defaults, TitleDialog, ListDialogMixin, NLSMixin,
    domConstruct, BaseList, config, typeIndex, localize) => {
  const ns = defaults.get('namespaces');

  const IdeasList = declare([BaseList], {
    nlsBundles: ['escoList', 'escaShowIdeas'],
    nlsCreateEntryMessage: null,
    includeRefreshButton: false,
    includeInfoButton: true,
    includeEditButton: false,
    includeRemoveButton: false,
    includeCreateButton: false,
    includeResultSize: false,
    entryType: ns.expand('esterms:Idea'),
    rowClickDialog: 'info',

    postCreate() {
      this.inherited('postCreate', arguments);
      this.listView.includeResultSize = this.includeResultSize;
    },

    getIconClass() {
      const typeConf = typeIndex.getConfByName('datasetIdea');// hardcoded entityname
      if (typeConf && typeConf.faClass) {
        return typeConf.faClass;
      }

      return '';
    },

    getEmptyListWarning() {
      return this.NLSBundle1.emptyListWarning;
    },
    getSearchObject() {
      const context = defaults.get('context');
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      return es.newSolrQuery().rdfType(this.entryType)
        .uriProperty('dcterms:source', this.entry.getResourceURI())
        .context(context.getResourceURI());
    },
    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem(
          config.catalog.datasetResultTemplateId);
      }
      return this.template;
    },
  });

  return declare([TitleDialog, ListDialogMixin, NLSMixin.Dijit], {
    nlsBundles: ['escaShowIdeas'],
    maxWidth: 800,
    includeFooter: false,

    postCreate() {
      this.ideasList = new IdeasList(null, domConstruct.create('div', null, this.containerNode));
      this.inherited(arguments);
    },
    open(params) {
      this.inherited(arguments);
      this.entry = params.row.entry;
      this.ideasList.entry = this.entry;
      this.ideasList.render();
      const title = defaults.get('rdfutils').getLabel(this.entry);
      this.updateLocaleStringsExplicit(localize(this.NLSBundle0, 'showIdeasDialogHeader', { 1: title }));
      this.show();
    },
  });
});

