define([
  'dojo/_base/declare',
  'entryscape-commons/defaults',
  'entryscape-commons/dialog/TitleDialog', // In template
  'entryscape-commons/list/common/ListDialogMixin',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'dojo/dom-construct',
  'entryscape-commons/list/common/BaseList',
  'config',
  'entryscape-commons/create/typeIndex',
  'di18n/localize',
  'i18n!nls/escaShowResults',
], (declare, defaults, TitleDialog, ListDialogMixin, _WidgetsInTemplateMixin, NLSMixin,
    domConstruct, BaseList, config, typeIndex, localize) => {
  const ns = defaults.get('namespaces');

  const ResultsList = declare([BaseList], {
    nlsBundles: ['escoList', 'escaShowResults'],
    nlsCreateEntryMessage: null,
    includeRefreshButton: false,
    includeInfoButton: true,
    includeEditButton: false,
    includeRemoveButton: false,
    includeCreateButton: false,
    includeResultSize: false,
    entryType: ns.expand('esterms:Result'),
    rowClickDialog: 'info',

    postCreate() {
      this.inherited('postCreate', arguments);
      this.listView.includeResultSize = this.includeResultSize;
    },

    getIconClass() {
      const typeConf = typeIndex.getConfByName('datasetResult');// hardcoded entityname
      if (typeConf && typeConf.faClass) {
        return typeConf.faClass;
      }

      return '';
    },

    getEmptyListWarning() {
      return this.NLSBundle1.emptyListWarning;
    },
    getSearchObject() {
      const context = defaults.get('context');
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      return es.newSolrQuery().rdfType(this.entryType)
        .uriProperty('dcterms:source', this.entry.getResourceURI())
        .context(context.getResourceURI());
    },
    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem(
          config.catalog.datasetResultTemplateId);
      }
      return this.template;
    },
  });

  return declare([TitleDialog, ListDialogMixin, NLSMixin.Dijit], {
    nlsBundles: ['escaShowResults'],
    maxWidth: 800,
    includeFooter: false,

    postCreate() {
      this.resultsList = new ResultsList(null, domConstruct.create('div', null, this.containerNode));
      this.inherited(arguments);
    },
    open(params) {
      this.inherited(arguments);
      this.entry = params.row.entry;
      this.resultsList.entry = this.entry;
      this.resultsList.render();
      const title = defaults.get('rdfutils').getLabel(this.entry);
      this.updateLocaleStringsExplicit(localize(this.NLSBundle0, 'showResultsDialogHeader', { 1: title }));
      this.show();
    },
  });
});

