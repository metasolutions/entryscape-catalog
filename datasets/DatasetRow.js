define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/string',
  'dojo/promise/all',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-construct',
  'entryscape-commons/comments/comments',
  'entryscape-commons/defaults',
  'entryscape-commons/list/common/ToggleRow',
  'di18n/localize',
  'config',
  './DistributionRow',
  'dojo/text!./DatasetRowTemplate.html',
], (declare, lang, array, string, all, domAttr, domStyle, domConstruct, comments, defaults,
    ToggleRow, localize, config, DistributionRow, template) =>

  declare([ToggleRow], {
    templateString: template,
    nlsPublicTitle: 'publicDatasetTitle',
    nlsProtectedTitle: 'privateDatasetTitle',

    postCreate() {
      this.inherited('postCreate', arguments);
      this.entry.getContext().getEntry().then((contextEntry) => {
        this.catalogPublic = contextEntry.isPublic();
        this.setToggled(contextEntry.isPublic(), this.entry.isPublic());
        this.renderCol4();
        if (this.nlsSpecificBundle) {
          this._updateLocaleStrings();
        }
      });
      this.listDistributions();
    },
    updateLocaleStrings(generic, specific) {
      this.inherited('updateLocaleStrings', arguments);
      this._updateLocaleStrings();
    },
    _updateLocaleStrings() {
      if (!this.catalogPublic) {
        domAttr.set(this.protectedNode, 'title', this.nlsSpecificBundle.privateDisabledDatasetTitle);
      } else {
        domAttr.set(this.protectedNode, 'title', this.nlsSpecificBundle[this.nlsProtectedTitle]);
      }
      domAttr.set(this.removeBrokenDatasetRefs, 'innerHTML', this.nlsSpecificBundle.removeBrokenDatasetRefs);
      domAttr.set(this.removeBrokenDatasetRefsWarning, 'innerHTML', this.nlsSpecificBundle.removeBrokenDatasetRefsWarning);
      this.maybeUpdate();
    },
    getDistributionStatements() {
      return this.entry.getMetadata().find(this.entry.getResourceURI(), 'dcat:distribution');
    },
    listDistributions() {
      const esu = defaults.get('entrystoreutil');
      this.fileEntryURIs = [];
      this.uri2Format = [];
      const self = this;
      const stmts = this.getDistributionStatements();
      all(array.map(stmts, (stmt) => {
        const ruri = stmt.getValue();
        return esu.getEntryByResourceURI(ruri).then((entry) => {
          const source = entry.getMetadata().findFirstValue(entry.getResourceURI(), 'dcterms:source');
          if (source !== '' && source != null) {
            self.fileEntryURIs.push(source);
          }
          const format = entry.getMetadata().findFirstValue(entry.getResourceURI(), 'dcterms:format');
          if (format !== '' && format != null) {
            // self.uri2Format[entry.getResourceURI()] = format;
            //let uri2FormatObj = {};
            self.uri2Format[entry.getResourceURI()] = format;
            //self.uri2Format.push(uri2FormatObj);
          }
          return entry;
        }, () => {
          domStyle.set(self.brokenReferences, 'display', '');
          // fail silently for missing distributions, list those that do exist.
          return null;
        });
      })).then(lang.hitch(this, this.showDistributionInList));
    },
    showDistributionInList(dists) {
      const distsArray = lang.isArray(dists) ? dists : [dists];
      distsArray.forEach((distE) => {
        if (distE != null) {
          DistributionRow({ entry: distE, datasetRow: this, dctSource: this.fileEntryURIs, uri2Format: this.uri2Format},
            domConstruct.create('tbody', null, this.distributions));
        }
      });
    },
    removeBrokenReferences() {
      const store = defaults.get('entrystore');
      const cache = store.getCache();
      const ns = defaults.get('namespaces');
      const md = this.entry.getMetadata();
      const datasetResourceURI = this.entry.getResourceURI();
      const stmts = this.getDistributionStatements();
      array.forEach(stmts, (stmt) => {
        const ruri = stmt.getValue();
        const euri = store.getEntryURI(store.getContextId(ruri), store.getEntryId(ruri));
        if (cache.get(euri) == null) {
          md.findAndRemove(datasetResourceURI, ns.expand('dcat:distribution'),
            { type: 'uri', value: ruri });
        }
      });
      this.entry.commitMetadata().then(() => {
        domStyle.set(this.brokenReferences, 'display', 'none');
      }, (err) => {
        alert(err);
      });
    },
    unpublishDataset(groupEntry, onSuccess) {
      const ei = this.entry.getEntryInfo();
      const acl = ei.getACL(true);
      acl.admin = acl.admin || [];
      acl.admin.push(groupEntry.getId());
      ei.setACL(acl);
      this.reRender();
      ei.commit().then(onSuccess);
      this.updateDistributionACL(acl);
    },
    toggleImpl(onSuccess) {
      const f = () => {
        const ns = defaults.get('namespaces');
        const ei = this.entry.getEntryInfo();
        const dialogs = defaults.get('dialogs');
        defaults.get('getGroupWithHomeContext')(this.entry.getContext()).then((groupEntry) => {
          if (groupEntry.canAdministerEntry()) {
            if (this.isPublicToggle) {
              const apiDistributionURIs = [];
              const esu = defaults.get('entrystoreutil');
              const stmts = this.getDistributionStatements();
              all(stmts.forEach((stmt) => {
                const ruri = stmt.getValue();
                esu.getEntryByResourceURI(ruri).then((entry) => {
                  const source = entry.getMetadata().findFirstValue(entry.getResourceURI(), ns.expand('dcterms:source'));
                  if (source !== '' && source != null) {
                    apiDistributionURIs.push(source);
                  }
                }, () => {
                }); // fail silently
              }));
              if (apiDistributionURIs.length === 0) {
                return this.unpublishDataset(groupEntry, onSuccess);
              }
              const confirmMessage = this.nlsSpecificBundle[this.list.nlsApiExistsToUnpublishDataset];
              return dialogs.confirm(confirmMessage, null, null, (confirm) => {
                if (confirm) {
                  return this.unpublishDataset(groupEntry, onSuccess);
                }
              });
            }
            ei.setACL({});
            this.reRender();
            ei.commit().then(onSuccess);
            this.updateDistributionACL({});
          } else {
            defaults.get('dialogs').acknowledge(this.nlsSpecificBundle.datasetSharingNoAccess);
          }
        });
      };

      if (this.isPublicToggle) {
        const es = defaults.get('entrystore');
        const adminRights = defaults.get('hasAdminRights');
        const userEntry = defaults.get('userEntry');
        const ccg = config.catalog.unpublishDatasetAllowedFor;
        const allowed = ccg === '_users' ? true :
          userEntry.getParentGroups().indexOf(es.getEntryURI('_principals', ccg)) >= 0;
        if (!adminRights && !allowed) {
          defaults.get('dialogs').acknowledge(this.nlsSpecificBundle.unpublishProhibited);
          return;
        }
      } else if (this.entry.getMetadata().find(null, 'dcat:distribution').length === 0) {
        const b = this.nlsSpecificBundle;
        defaults.get('dialogs').confirm(
          b.confirmPublishWithoutDistributions,
          b.proceedPublishWithoutDistributions,
          b.cancelPublishWithoutDistributions).then(f);
        return;
      }
      f();
    },
    action_remove() {
      const self = this;
      const dialogs = defaults.get('dialogs');
      const store = defaults.get('entrystore');
      const ns = defaults.get('namespaces');
      const cache = store.getCache();
      const stmts = this.getDistributionStatements();

      // check for filedistribution/api distrubtions
      const esu = defaults.get('entrystoreutil');
      const fileDistributionURIs = [];
      const apiDistributionURIs = [];
      const baseURI = store.getBaseURI();

      const es = defaults.get('entrystore');
      all(stmts.map((stmt) => {
        const ruri = stmt.getValue();
        return esu.getEntryByResourceURI(ruri).then((entry) => {
          const source = entry.getMetadata().findFirstValue(entry.getResourceURI(), ns.expand('dcterms:source'));
          const downloadURI = entry.getMetadata().findFirstValue(entry.getResourceURI(), ns.expand('dcat:downloadURL'));

          if (source !== '' && source != null) {
            apiDistributionURIs.push(source);
          }
          if (downloadURI !== '' && downloadURI != null && downloadURI.indexOf(baseURI) > -1) {
            fileDistributionURIs.push(downloadURI);
          }
        }, () => {
        }); // fail silently
      })).then(() => {
        if (apiDistributionURIs.length > 0 && fileDistributionURIs.length > 0) {
          dialogs.acknowledge(this.nlsSpecificBundle.removeDatasetWithFileAndApiDistributions);
          return;
        } else if (apiDistributionURIs.length > 0) {
          dialogs.acknowledge(this.nlsSpecificBundle.removeFileDistAPI);
          return;
        } else if (fileDistributionURIs.length > 0) {
          dialogs.acknowledge(this.nlsSpecificBundle.removeDatasetWithFileDistributions);
          return;
        }
        if (apiDistributionURIs.length === 0 && fileDistributionURIs.length === 0) {
          let confirmMessage;
          if (stmts.length > 0) {
            if (self.noOfComments > 0) {
              confirmMessage = localize(this.nlsSpecificBundle, 'removeDatasetDistributionsAndCommentsConfirm',
                { distributions: stmts.length, comments: self.noOfComments });
            } else {
              confirmMessage = localize(this.nlsSpecificBundle,
                'removeDatasetAndDistributionsQuestion', stmts.length);
            }
          } else if (self.noOfComments > 0) {
            confirmMessage = localize(this.nlsSpecificBundle, 'removeDatasetAndCommentsConfirm', self.noOfComments);
          } else {
            confirmMessage = this.nlsSpecificBundle.removeDatasetQuestion;
          }
          dialogs.confirm(confirmMessage, null, null, (confirm) => {
            if (!confirm) {
              return;
            }
            const dists = array.filter(array.map(stmts, (stmt) => {
              const ruri = stmt.getValue();
              return cache.getByResourceURI(ruri);
            }), dist => dist.length > 0);

            es.newSolrQuery()
              .uriProperty('oa:hasTarget', this.entry.getResourceURI()).rdfType('oa:Annotation')
              .list()
              .getEntries(0)
              .then((allComments) => {
                all(array.map(allComments, comment => comment.del())).then(() => {
                  all(array.map(dists, dist => dist[0].del())).then(() => {
                    const resURI = this.entry.getResourceURI();
                    return this.entry.del().then(() => {
                      this.list.getView().removeRow(this);
                      this.destroy();
                    }).then(() => defaults.get('entrystoreutil').getEntryByType('dcat:Catalog', this.entry.getContext())
                      .then((catalog) => {
                        catalog.getMetadata().findAndRemove(null, ns.expand('dcat:dataset'), {
                          value: resURI,
                          type: 'uri',
                        });
                        return catalog.commitMetadata();
                      }));
                  }, () => {
                    domAttr.set(this.distributions, 'innerHTML', '');
                    this.listDistributions();
                    dialogs.acknowledge(this.nlsSpecificBundle.failedToRemoveDatasetDistributions);
                  });
                });
              });
          });
        }
      });
    },
    action_preview() {
      /**
       * Encoded resource URI; base64 used
       * @type {string}
       */
      const dataset = this.entry.getId();
      if (config.catalog && config.catalog.previewURLNewWindow) {
        window.open(url, '_blank');
      } else {
        const site = defaults.get('siteManager');
        const state = site.getState();
        const { context } = state[state.view];
        site.render('catalog__datasets__preview', { context, dataset });
      }
    },
    clearDistributions() {
      domConstruct.empty(this.distributions);
    },
    /**
     * Update all distributions of the current dataset with an ACL.
     *
     * @param acl ACL object
     */
    updateDistributionACL(acl) {
      const stmts = this.getDistributionStatements();
      stmts.forEach((stmt) => {
        const esu = defaults.get('entrystoreutil');
        const resourceURI = stmt.getValue();

        esu.getEntryByResourceURI(resourceURI).then((resourceEntry) => {
          if (acl) {
            const resourceEntryInfo = resourceEntry.getEntryInfo();
            resourceEntryInfo.setACL(acl);
            resourceEntryInfo.commit();
          }
        });
      });
    },
    renderCol4() {
      // badgeNode
      comments.getNrOfComments(this.entry).then((nr) => {
        this.noOfComments = nr;
        if (nr > 0) {
          domStyle.set(this.badgeNode, 'display', '');
          domAttr.set(this.badgeNode, 'innerHTML', nr);
          this.maybeUpdate();
        }
      });
    },

    decreaseReplyCount() {
      this.noOfComments -= 1;
      this.renderCommentCount();
    },

    renderCommentCount() {
      // badgeNode
      if (this.noOfComments > 0) {
        domStyle.set(this.badgeNode, 'display', '');
        domAttr.set(this.badgeNode, 'innerHTML', this.noOfComments);
      } else {
        domStyle.set(this.badgeNode, 'display', 'none');
      }
      this.maybeUpdate();
    },
    maybeUpdate() {
      if (this.nlsSpecificBundle) {
        if (this.noOfComments > 0) {
          const tStr = localize(this.nlsSpecificBundle, 'commentTitle', this.noOfComments);
          domAttr.set(this.badgeNode, 'title', tStr);
        }
      }
    },
    openCommentDialog(ev) {
      this.list.openDialog('comment', { row: this });
      ev.stopPropagation();
    },
  }));
