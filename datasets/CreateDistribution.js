define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-construct',
  'rdforms/model/validate',
  'entryscape-commons/rdforms/RDFormsEditDialog',
  'entryscape-commons/create/EntryType',
  'entryscape-commons/defaults',
  'dojo/dom-attr',
  'config',
  'i18n!nls/escoEntryType',
  'i18n!nls/escoRdforms',
  'i18n!nls/escaDataset',
], (declare, lang, domConstruct, validate, RDFormsEditDialog, EntryType, defaults,
    domAttr, config) => {
  const ns = defaults.get('namespaces');
  const distributionEntryType = declare([EntryType], {
    nlsBundles: ['escoEntryType', 'escaDataset'],
    localeChange() {
      this.inherited(arguments);
      domAttr.set(this.__fileOptionLabelNLS, 'innerHTML', this.NLSBundles.escaDataset.fileUploadDistribution);
      domAttr.set(this.__linkOptionLabelNLS, 'innerHTML', this.NLSBundles.escaDataset.accessURIDistribution);
      domAttr.set(this.__linkLabel, 'innerHTML', this.NLSBundles.escaDataset.accessURIDistribution);
    },
    fileOption(ev) {
      if (config.catalog && config.catalog.disallowFileuploadDistributionDialog) {
        defaults.get('dialogs').restriction(config.catalog.disallowFileuploadDistributionDialog);
        ev.preventDefault();
        ev.stopPropagation();
      }
      this.inherited(arguments);
    },
  });
  return declare([RDFormsEditDialog], {
    nlsBundles: ['escoRdforms', 'escaDataset'],
    title: 'temp',
    nlsHeaderTitle: 'createDistributionHeader',
    nlsFooterButtonLabel: 'createDistributionButton',
    explicitNLS: true,
    postCreate() {
      if (!config.catalog || config.catalog.distributionTemplateCreate !== true) {
        this.fileOrLink = new distributionEntryType({
          optionChange: lang.hitch(this, function () {
            if (this.fileOrLink.isFile()) {
              this.editor.filterPredicates = {
                'http://www.w3.org/ns/dcat#accessURL': true,
                'http://www.w3.org/ns/dcat#downloadURL': true,
              };
            } else {
              this.editor.filterPredicates = { 'http://www.w3.org/ns/dcat#accessURL': true };
            }
            this.editor.render();
          }),
          valueChange: lang.hitch(this, function (value) {
            if (value != null) {
              this.unlockFooterButton();
            } else {
              this.lockFooterButton();
            }
          }),
          list: this.list,
        }, domConstruct.create('div', null, this.containerNode, 'first'));
      }
      this.inherited(arguments);
      this.editor.render();
    },
    updateGenericCreateNLS() {
      this.title = this.NLSBundles.escaDataset[this.nlsHeaderTitle];
      this.doneLabel = this.NLSBundles.escaDataset[this.nlsFooterButtonLabel];
      this.updateTitleAndButton();
    },
    open(params) {
      this.row = params.row;
      this.datasetEntry = params.row.entry;
      if (this.fileOrLink) {
        this.fileOrLink.show(config.catalog.excludeFileuploadDistribution !== true, true, false);
        this.editor.filterPredicates = { 'http://www.w3.org/ns/dcat#accessURL': true };
      }
      this._newEntry = defaults.get('createEntry')(null, 'dcat:Distribution');
      const nds = this._newEntry;
      nds.getMetadata().add(nds.getResourceURI(), ns.expand('rdf:type'), ns.expand('dcat:Distribution'));
      this.updateGenericCreateNLS();
      this.show(nds.getResourceURI(), nds.getMetadata(), this.getDistributionTemplate());
    },
    getDistributionTemplate() {
      if (!this.dtemplate) {
        this.dtemplate = defaults.get('itemstore').getItem(
          config.catalog.distributionTemplateId);
      }
      return this.dtemplate;
    },
    getReport() {
      const report = validate.bindingReport(this.editor.binding);
      if (this.fileOrLink && report.errors.length > 0) {
        report.errors = report.errors.filter(err => !(err.item && err.item.getProperty() === ns.expand('dcat:accessURL')));
      }
      return report;
    },

    doneAction(graph) {
      const context = defaults.get('context');
      const pDistributionEntry = this._newEntry;
      this._setACL(pDistributionEntry);
      const createAndConnect = () => pDistributionEntry.setMetadata(graph).commit()
          .then((distributionEntry) => {
            this.datasetEntry.getMetadata().add(this.datasetEntry.getResourceURI(), 'dcat:distribution', distributionEntry.getResourceURI());
            return this.datasetEntry.commitMetadata().then(() => {
              this.row.clearDistributions();
              this.row.listDistributions();
              distributionEntry.setRefreshNeeded();
              return distributionEntry.refresh();
            });
          }, () => {
            throw this.NLSBundles.escaDataset.createDistributionErrorMessage;
          });
      const distResourceURI = pDistributionEntry.getResourceURI();
      if (this.fileOrLink) {
        if (this.fileOrLink.isFile()) {
          const pFileEntry = context.newEntry();
          const md = pFileEntry.getMetadata();
          const pfileURI = pFileEntry.getResourceURI();
          md.add(pfileURI, 'rdf:type', 'esterms:File');
          md.addL(pfileURI, 'dcterms:title', this.fileOrLink.getValue());
          return pFileEntry.commit().then(fileEntry => fileEntry.getResource(true)
            .putFile(this.fileOrLink.getFileInputElement())
            .then(() => fileEntry.refresh().then(() => {
              const fileResourceURI = fileEntry.getResourceURI();
              graph.add(distResourceURI, 'dcat:accessURL', fileResourceURI);
              graph.add(distResourceURI, 'dcat:downloadURL', fileResourceURI);
              const format = fileEntry.getEntryInfo().getFormat();
              const manualFormatList = graph.find(distResourceURI, 'dcterms:format');
              if (typeof format !== 'undefined' && manualFormatList.length === 0) {
                graph.addL(distResourceURI, 'dcterms:format', format);
              }
              return createAndConnect();
            })));
        }
        const uri = this.fileOrLink.getValue();
        graph.add(distResourceURI, 'dcat:accessURL', uri);
      }
      return createAndConnect();
    },
    _setACL(distributionEntry) {
      const datasetACL = this.datasetEntry.getEntryInfo().getACL();
      distributionEntry.getEntryInfo().setACL(datasetACL);
    },
  });
});
