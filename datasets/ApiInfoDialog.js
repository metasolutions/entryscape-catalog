define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  './api',
  './pipelineUtil',
  'entryscape-commons/defaults',
  'entryscape-commons/dialog/TitleDialog', // In template
  'dojo/text!./ApiInfoDialogTemplate.html',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'dojo/Deferred',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/on',
  'i18n!nls/escaFiles',
  'i18n!nls/escaDataset',
], (declare, lang, api, pipelineUtil, defaults, TitleDialog, template, _WidgetsInTemplateMixin,
    NLSMixin, Deferred, domAttr, domStyle, domConstruct, on) =>
  declare([TitleDialog.Content, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    bid: 'escaApiInfo',
    templateString: template,
    nlsBundles: ['escaFiles', 'escaDataset'],

    maxWidth: 800,
    nlsHeaderTitle: 'apiInfoDialogHeader',
    nlsFooterButtonLabel: 'apiInfoDialogCloseLabel',
    nlsFooterButtonTitle: 'apiInfoDialogfooterButtonTitle',
    includeFooter: false,
    title: 'apiInfoDialogTitle',

    postCreate() {
      this.inherited(arguments);
      // domClass.add(this.saveButton, 'disabled');
      // domClass.add(this.removeButton, 'disabled');
      domAttr.set(this.removeButton, 'disabled', true);
      domAttr.set(this.saveButton, 'disabled', true);
      let t;
      const checkAliasName = lang.hitch(this, this.checkAliasName);
      domStyle.set(this.__aliasError, 'display', 'none');
      on(this.apiAlias, 'keyup', () => {
        if (t != null) {
          clearTimeout(t);
        }
        t = setTimeout(checkAliasName, 300);
      });
    },
    checkAliasName() {
      // TODO check allowed chars
      domStyle.set(this.__aliasError, 'display', 'none');
      const aliasName = domAttr.get(this.apiAlias, 'value');
      if (aliasName === '' || aliasName === this.currentAliasName) {
        // domClass.add(this.saveButton, 'disabled');
        domAttr.set(this.saveButton, 'disabled', true);
      } else {
        const alphanum = /^[0-9a-zA-Z]+$/;
        if (!aliasName.match(alphanum)) {
          // domClass.add(this.saveButton, 'disabled');
          domAttr.set(this.saveButton, 'disabled', true);
          domStyle.set(this.__aliasError, 'display', '');
          domAttr.set(this.__aliasError, 'innerHTML', this.NLSBundles.escaDataset.invalidAliasName);
          return;
        }
        // domClass.remove(this.saveButton, 'disabled');
        domAttr.set(this.saveButton, 'disabled', false);
      }
    },

    saveAlias() {
      const aliasName = domAttr.get(this.apiAlias, 'value');
      pipelineUtil.setAlias(this.etlEntry, aliasName).then(() => {
        this.currentAliasName = aliasName;
        domAttr.set(this.removeButton, 'disabled', false);
        this._setAliasNameInExternalMetadata(aliasName);
        this._updateExampleURL(aliasName);
      }, (err) => {
        if (err && err.response.status === 400) {
          domStyle.set(this.__aliasError, 'display', '');
          domAttr.set(this.__aliasError, 'innerHTML', this.NLSBundles.escaDataset.duplicateAliasName);
        }
      });
    },
    removeAlias() {
      pipelineUtil.removeAlias(this.etlEntry).then(() => {
        domAttr.set(this.apiAlias, 'value', '');
        domAttr.set(this.removeButton, 'disabled', true);
        this.currentAliasName = domAttr.get(this.apiAlias, 'value');
        this._setAliasNameInExternalMetadata();
        this._updateExampleURL();
      });
    },
    localeChange() {
      this.dialog.updateLocaleStrings(this.NLSBundles.escaDataset);
    },
    open(params) {
      this.etlEntry = params.etlEntry;
      this.apiDistributionEntry = params.apiDistributionEntry;
      // this.datasetEntry = params.datasetEntry;
      // this.currentAliasName = 'test';
      const rURI = this.etlEntry.getResourceURI();
      const apiId = rURI.substr(rURI.lastIndexOf('/') + 1, rURI.length);
      domAttr.set(this.apiId, 'innerHTML', apiId);
      domAttr.set(this.apiAlias, 'value', '');
      // domClass.add(this.removeButton, 'disabled');
      domAttr.set(this.removeButton, 'disabled', true);
      this._clearRows();
      this.detectAPI();
      // domClass.add(this.saveButton, 'disabled');
      domAttr.set(this.saveButton, 'disabled', true);
      domStyle.set(this.__aliasError, 'display', 'none');
      this.dialog.show();
    },
    _clearRows() {
      domAttr.set(this.apiColumnsTableBody, 'innerHTML', '');
    },
    _updateExampleURL(alias) {
      // http://localhost:8282/dataset/4df514c2-1669-4b96-b206-79f4cba606ea?first name=some_string_pattern
      let exampleURL;
      // const test = `${this.etlEntry.getResourceURI()}?${alias}=some_string_pattern`;
      if (alias) {
        const str = this.etlEntry.getResourceURI();
        exampleURL = str.substring(0, str.lastIndexOf('/') + 1) + alias;
        exampleURL = `${exampleURL}?${this.colName}=some_string_pattern`;
      } else {
        exampleURL = `${this.etlEntry.getResourceURI()}?${this.colName}=some_string_pattern`;
      }

      domAttr.set(this.searchExampleLink, 'innerHTML', exampleURL);
      domAttr.set(this.searchExampleLink, 'href', exampleURL);
    },
    _renderRows() {
      const stmts = this.etlEntry.getCachedExternalMetadata().find(null, 'store:pipelineResultColumnName');
      const cols = stmts.map(stmt => stmt.getValue());
      if (cols.length === 0) {
        domAttr.set(this.apiStatus, 'innerHTML',
          this.NLSBundles.escaFiles.apiNotConnected);
        domStyle.set(this.apiInfo, 'display', 'none');
        domStyle.set(this.__apiRefreshButton, 'display', '');
      } else {
        domAttr.set(this.apiStatus, 'innerHTML', this.NLSBundles.escaFiles.apiStatus_available);
        domStyle.set(this.__apiRefreshButton, 'display', 'none');
        domStyle.set(this.apiStatus, 'color', 'green');
        domStyle.set(this.apiInfo, 'display', '');

        // getting alias Name
        this.colName = cols[0];
        const aliasName = this.etlEntry.getCachedExternalMetadata().findFirstValue(null, 'store:aliasName');
        if (aliasName) {
          domAttr.set(this.apiAlias, 'value', aliasName);
          this.currentAliasName = aliasName;
          this._updateExampleURL(aliasName);
        } else {
          this._updateExampleURL();
        }
        if (this.currentAliasName) { // check for
          // domClass.remove(this.removeButton, 'disabled');
          domAttr.set(this.removeButton, 'disabled', false);
        }
        /*
        const exampleURL = `${this.etlEntry.getResourceURI()}?${cols[0]}=some_string_pattern`;
        domAttr.set(this.searchExampleLink, 'innerHTML', exampleURL);
        domAttr.set(this.searchExampleLink, 'href', exampleURL);
         */
        cols.forEach((column) => {
          const tr = domConstruct.create('tr', null, this.apiColumnsTableBody);
          domConstruct.create('td', { innerHTML: column }, tr);
        });
      }
    },
    refreshAPIStatus() {
      this.detectAPI();
    },
    _setAliasNameInExternalMetadata(aliasName) {
      this.getAPIStatus(this.etlEntry).then((status) => {
        if (status === 'available') {
          api.updateAliasInEntry(this.etlEntry, aliasName);
        }
      });
    },
    detectAPI() {
      domStyle.set(this.__apiRefreshButton, 'display', 'none');
      domStyle.set(this.apiInfo, 'display', '');
      domStyle.set(this.apiStatus, 'color', 'black');
      this.getAPIStatus(this.etlEntry)
        .then((status) => {
          const statusMessageKey = `apiStatus_${status}`;
          domAttr.set(this.apiStatus, 'innerHTML',
            this.NLSBundles.escaFiles[statusMessageKey]);
          switch (status) {
            case 'error':
              domStyle.set(this.apiStatus, 'color', 'red');
              domStyle.set(this.apiInfo, 'display', 'none');
              domStyle.set(this.__apiRefreshButton, 'display', 'none');
              break;
            case 'available':
              domStyle.set(this.apiStatus, 'color', 'green');
              domStyle.set(this.apiInfo, 'display', '');
              this._clearRows();
              this._renderRows();
              break;
            default:
              domStyle.set(this.apiStatus, 'color', 'orange');
              domStyle.set(this.__apiRefreshButton, 'display', '');
              domStyle.set(this.apiInfo, 'display', 'none');
          }
        });
    },
    getAPIStatus(etlEntry) {
      etlEntry.setRefreshNeeded();
      return etlEntry.refresh().then(() => {
        const oldStatus = api.oldStatus(etlEntry);
        if (oldStatus != null) {
          return oldStatus;
        }
        return api.load(etlEntry).then(data => api.update(etlEntry, data).then(() =>
          api.status(data)));
      });
    },
  }));
