define([
  'entryscape-commons/defaults',
  'entryscape-commons/create/typeIndex',
  'config',
], (defaults, typeIndex, config) => {
  const ns = defaults.get('namespaces');
  ns.add('dcat', 'http://www.w3.org/ns/dcat#');
  ns.add('esterms', 'http://entryscape.com/terms/');

  defaults.set('withinDatasetLimit', (count) => {
    if (!defaults.get('hasAdminRights') &&
      config.catalog && parseInt(config.catalog.datasetLimit, 10) === config.catalog.datasetLimit) {
      let exception = false;
      const premiumGroupId = config.entrystore.premiumGroupId;
      if (premiumGroupId) {
        const es = defaults.get('entrystore');
        const groups = defaults.get('userEntry').getParentGroups();
        exception = groups.some(groupEntryURI => es.getEntryId(groupEntryURI) === premiumGroupId);
      }
      const premiumContextLevel = defaults.get('context')
        .getEntry(true)
        .getEntryInfo()
        .getGraph()
        .findFirstValue(null, 'store:premium');

      if (premiumContextLevel) {
        exception = true;
      }
      return exception || count < parseInt(config.catalog.datasetLimit, 10);
    }
    return true;
  });

  // Copy over templates from catalog config to corresponding entitytypes
  const entities = ['publisher', 'catalog', 'dataset', 'distribution', 'contactPoint', 'datasetResult'];
  entities.forEach((e) => {
    const t = config.catalog[`${e}TemplateId`];
    const conf = typeIndex.getConfByName(e);
    if (t && conf) {
      conf.template = t;
    }
  });

  return defaults;
});
