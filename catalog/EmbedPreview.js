define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/on',
  'entryscape-commons/defaults',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/list/common/ListDialogMixin',
  'i18n!nls/escaEmbedPreview',
], (declare, lang, domAttr, domStyle, domConstruct, on, defaults,
    TitleDialog, ListDialogMixin) => declare([TitleDialog.ContentNLS, ListDialogMixin], {
      maxWidth: 800,
      nlsBundles: ['escaEmbedPreview'],
      nlsHeaderTitle: 'embedPreviewHeader',
      includeFooter: false,
      bid: 'escaEmbedPreview',
      postCreate() {
        domStyle.set(this.dialog.containerNode, 'height', '100%');
        domConstruct.empty(this.dialog.containerNode);
        this.inherited(arguments);
      },
      open(params) {
        this.inherited(arguments);
        if (this.iframe) {
          domConstruct.destroy(this.iframe);
        }
        this.iframe = domConstruct.create('iframe', { class: `${this.bid}__embedCode` }, this.dialog.containerNode);
        const embedCode = params.embededCode;
        const html = `<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>${embedCode}</body></html>`;
        setTimeout(() => {
          this.iframe.contentWindow.document.open();
          this.iframe.contentWindow.document.write(html);
          this.iframe.contentWindow.document.close();
        }, 1);
        this.dialog.show();
      },
    }));
