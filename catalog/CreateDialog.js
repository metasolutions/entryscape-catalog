define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'config',
  'rdforms/view/renderingContext',
  'entryscape-commons/defaults',
  'entryscape-commons/dialog/TitleDialog',
  'dojo/text!./CreateDialogTemplate.html',
  'entryscape-commons/list/common/ListDialogMixin',
  'i18n!nls/escaCatalog',
], (declare, domAttr, domStyle, _WidgetsInTemplateMixin, NLSMixin, config, renderingContext,
    defaults, TitleDialog, template, ListDialogMixin) => {
  const createWithoutPublisher = config.catalog && config.catalog.createWithoutPublisher === true;

  return declare(
    [TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin.Dijit], {
      templateString: template,
      maxWidth: 800,
      nlsBundles: ['escaCatalog'],
      nlsHeaderTitle: 'createCatalogHeader',
      nlsFooterButtonLabel: 'createCatalogButton',

      postCreate() {
        if (createWithoutPublisher) {
          domStyle.set(this.publisherNode, 'display', 'none');
        }
        this.inherited(arguments);
      },
      open() {
        if (!defaults.get('hasAdminRights')
          && config.catalog
          && parseInt(config.catalog.catalogLimit, 10) === config.catalog.catalogLimit
          && config.catalog.catalogLimitDialog) {
          let exception = false;
          const premiumGroupId = config.entrystore.premiumGroupId;
          if (premiumGroupId) {
            const es = defaults.get('entrystore');
            const groups = defaults.get('userEntry').getParentGroups();
            exception =
              groups.some(groupEntryURI => es.getEntryId(groupEntryURI) === premiumGroupId);
          }

          if (!exception &&
            this.list.getView().getSize() >= parseInt(config.catalog.catalogLimit, 10)) {
            defaults.get('dialogs').restriction(config.catalog.catalogLimitDialog);
            return;
          }
        }
        this.list.getView().clearSearch();

        domAttr.set(this.catalogName, 'value', '');
        domAttr.set(this.catalogDesc, 'value', '');
        domAttr.set(this.agentName, 'value', '');
        this.dialog.show();
      },
      footerButtonAction() {
        let group;
        let hc;
        const name = domAttr.get(this.catalogName, 'value');
        const desc = domAttr.get(this.catalogDesc, 'value');
        const publisher = domAttr.get(this.agentName, 'value');
        const store = defaults.get('entrystore');
        if ((!createWithoutPublisher && publisher === '') || desc === '' || name === '') {
          return this.NLSBundles.escaCatalog.insufficientInfoToCreateCatalog;
        }
        let context;
        return store.createGroupAndContext()
          .then((entry) => {
            group = entry;
            hc = entry.getResource(true).getHomeContext();
            context = store.getContextById(hc);
            if (createWithoutPublisher) {
              return entry;
            }
            const ae = context.newNamedEntry();
            const md = ae.getMetadata();
            md.add(ae.getResourceURI(), 'rdf:type', 'foaf:Agent');
            md.addL(ae.getResourceURI(), 'foaf:name', publisher);
            return ae.commit();
          }).then((publisherEntry) => {
            const pe = defaults.get('createEntry')(context, 'dcat:Catalog');
            const md = pe.getMetadata();
            const subj = pe.getResourceURI();
            const l = renderingContext.getDefaultLanguage();
            md.add(subj, 'rdf:type', 'dcat:Catalog');
            md.addL(subj, 'dcterms:title', name, l);
            md.addL(subj, 'dcterms:description', desc, l);
            if (!createWithoutPublisher) {
              md.add(subj, 'dcterms:publisher', publisherEntry.getResourceURI());
            }

            return pe.commit();
          }).then(dcat => context.getEntry().then((ctxEntry) => {
            const hcEntryInfo = ctxEntry.getEntryInfo();
            hcEntryInfo.getGraph().add(ctxEntry.getResourceURI(), 'rdf:type', 'esterms:CatalogContext');
            // TODO remove when entrystore is changed so groups have read
            // access to homecontext metadata by default.
            // Start fix with missing metadata rights on context for group
            const acl = hcEntryInfo.getACL(true);
            acl.mread.push(group.getId());
            hcEntryInfo.setACL(acl);
            // End fix
            return hcEntryInfo.commit().then(() => {
              if (this.list.entryList) { // Not available (and not needed) if admin.
                this.list.entryList.setGroupIdForContext(context.getId(), group.getId());
              }
              const row = this.list.getView().addRowForEntry(dcat);
              this.list.rowMetadataUpdated(row);
              const userEntry = defaults.get('userEntry');
              userEntry.setRefreshNeeded();
              userEntry.refresh();
            });
          }), (err) => {
            // dialogs.acknowledge(err);
            throw err;
          });
      },
    });
});
