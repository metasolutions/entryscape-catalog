define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'entryscape-commons/defaults',
  './Dataset', // In template
  'entryscape-commons/dialog/HeaderDialog', // In template
  'entryscape-commons/rdforms/linkBehaviour',
  'dojo/text!./DatasetDialogTemplate.html',
], (declare, domAttr, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, defaults, Dataset,
    HeaderDialog, linkBehaviour, template) =>

  declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
    templateString: template,

    open(params) {
      this.entry = params.row.entry;
      this.dataset.showDataset(params.row.entry);
      domAttr.set(this.titleNode, 'innerHTML', defaults.get('rdfutils').getLabel(this.entry));
      this.dialog.show();
    },
  }));
