define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'entryscape-commons/defaults',
  'rdforms/view/Presenter', // In template
  'entryscape-commons/dialog/HeaderDialog', // In template
  'entryscape-catalog/files/APIInfo', // In template
  'entryscape-commons/rdforms/RDFormsPresentDialog',
  'config',
  'dojo/text!./DistributionDialogTemplate.html',
], (declare, lang, defaults, Presenter, HeaderDialog, APIInfo, RDFormsPresentDialog, config,
    template) =>

  declare([RDFormsPresentDialog], {
    templateString: template,
    checkForAPI: true,

    open(entry) {
      if (this.checkForAPI) {
        this.updateApiInfo(entry);
      }
      this.show(entry.getResourceURI(), entry.getMetadata(),
        defaults.get('itemstore').getItem(config.catalog.distributionTemplateId));
    },
    updateApiInfo(entry) {
      this.apiInfo.hide();
      const md = entry.getMetadata();
      const es = defaults.get('entrystore');
      const ns = defaults.get('namespaces');
      const fileEntryResourceURI = md.findFirstValue(entry.getResourceURI(), ns.expand('dcterms:source'));
      if (fileEntryResourceURI != null && fileEntryResourceURI.indexOf(es.getBaseURI()) === 0) {
        const fileEntryURI = es.getEntryURI(
          es.getContextId(fileEntryResourceURI), es.getEntryId(fileEntryResourceURI));

        es.getEntry(fileEntryURI)
          .then((fileEntry) => {
            const pipelineResults = fileEntry.getReferrers(ns.expand('store:pipelineData'));
            if (pipelineResults.length > 0) {
              es.getEntry(pipelineResults[0]).then((pipelineResult) => {
                this.apiInfo.show(pipelineResult);
              });
            }
          });
      }
    },
  }));
