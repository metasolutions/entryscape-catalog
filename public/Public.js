define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-construct',
  'dijit/_WidgetBase',
  'entryscape-commons/defaults',
  'entryscape-catalog/public/Dataset',
  'entryscape-commons/rdforms/linkBehaviour',
], (declare, lang, domConstruct, _WidgetBase, defaults, Dataset) => {

  return declare([_WidgetBase], {
    buildRendering() {
      this.domNode = this.srcNodeRef || domConstruct.create('div');
      this.viewNode = domConstruct.create('div', null, this.domNode);
    },

    show(params) {
      const { context, dataset } = params.params;
      const es = defaults.get('entrystore');
      const contextObj = context ? es.getContextById(context) : defaults.get('context');

      if (dataset) {
        const entrystoreutil = defaults.get('entrystoreutil');
        this.entryPromise = es.getEntry(es.getEntryURI(contextObj.getId(), dataset));
        this.entryPromise.then(lang.hitch(this, this.showEntry));
      }
    },

    showEntry(entry) {
      if (this.viewer) {
        this.viewer.destroy();
        delete this.viewer;
      }
      this.viewer = new Dataset({ inDialog: false }, domConstruct.create('div', null, this.viewNode));
      this.viewer.startup();
      this.viewer.showEntry(entry);
    },
  });
});
