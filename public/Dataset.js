define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/Deferred',
  'dojo/on',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/promise/all',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'rdforms/model/system',
  'entryscape-commons/defaults',
  'config',
  'rdforms/view/Presenter', // In template
  'entryscape-commons/rdforms/RDFormsPresentDialog', // In template
  'entryscape-catalog/public/DistributionDialog', // In template
  'entryscape-commons/rdforms/linkBehaviour',
  'dojo/text!./DatasetTemplate.html',
  'i18n!nls/escaPublic',
], (declare, lang, Deferred, on, domAttr, domConstruct, domClass, domStyle, all,
    _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin, system, defaults,
    config, Presenter, RDFormsPresentDialog, DistributionDialog, linkBehaviour, template) =>

  declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    templateString: template,
    inDialog: false,
    nlsBundles: ['escaPublic'],

    postCreate() {
      this.inherited('postCreate', arguments);

      if (!this.inDialog) {
        domClass.add(this.domNode, 'mainView');
        domConstruct.place(this.detailsBlock, this.details);
        domConstruct.place(this.distributionBlock, this.sidebar);
        domConstruct.place(this.catalogBlock, this.sidebar);
        domConstruct.place(this.resultBlock, this.sidebar);
      }
    },
    show(params) {
      const store = defaults.get('entrystore');
      const context = defaults.get('context');
      const entryId = params.entry;
      if (context && entryId) {
        this.entryPromise = store.getEntry(store.getEntryURI(context.getId(), entryId));
        this.entryPromise.then(lang.hitch(this, this.showDataset));
      }
    },
    showEntry(entry) {
      this.entryPromise = new Deferred();
      this.entryPromise.resolve(entry);
      this.showDataset(entry);
    },

    localeChange() {
      if (this.entryPromise) {
        this.entryPromise.then(lang.hitch(this, this.showDataset));
      }
    },

    showDataset(entry) {
      // defaults.get('itemstore', (itemstore) => {
      const itemstore = defaults.get('itemstore');
      this.entry = entry;
      const rdfutils = defaults.get('rdfutils');
      const datasetTemplate = itemstore.getItem(config.catalog.datasetTemplateId);
      this.presenter.show({
        resource: entry.getResourceURI(),
        graph: entry.getMetadata(),
        template: datasetTemplate,
      });

      this.fetchCatalog(entry).then((catalogEntry) => {
        domAttr.set(this.catalog, 'innerHTML', rdfutils.getLabel(catalogEntry));
        this.dcatEntry = catalogEntry;
      });

      this.fetchDistributions(entry).then(lang.hitch(this, this.showDistributions));
      this.fetchResults(entry).then(lang.hitch(this, this.showResults));
      // });
    },

    fetchCatalog(datasetEntry) {
      /** @type {store/EntryStore} */
      const store = defaults.get('entrystore');
      return store.newSolrQuery().rdfType('dcat:Catalog').limit(2)
        .context(datasetEntry.getContext())
        .getEntries(0)
        .then((entryArr) => {
          if (entryArr.length !== 1) {
            throw Error('Wrong number of entries.');
          }
          return entryArr[0];
        });
    },

    fetchDistributions(datasetEntry) {
      const storeutil = defaults.get('entrystoreutil');
      const md = datasetEntry.getMetadata();
      const stmts = md.find(datasetEntry.getResourceURI(), 'dcat:distribution');
      return all(stmts.map(
        stmt => storeutil.getEntryByResourceURI(stmt.getValue()).then(entry => entry)));
    },

    fetchResults(datasetEntry) {
      const list = [];
      return defaults.get('entrystore').newSolrQuery().rdfType('esterms:Result')
        .context(datasetEntry.getContext())
        .uriProperty('dcterms:source', datasetEntry.getResourceURI())
        .list()
        .forEach((result) => {
          list.push(result);
        })
        .then(() => list);
    },

    showDistributions(dists) {
      domAttr.set(this.distributions, 'innerHTML', '');
      dists.forEach((distE) => {
        if (distE != null) {
          const md = distE.getMetadata();
          const subj = distE.getResourceURI();
          const title = md.findFirstValue(subj, 'http://purl.org/dc/terms/title');
          const desc = md.findFirstValue(subj, 'http://purl.org/dc/terms/description');
          const access = md.findFirstValue(subj, 'http://www.w3.org/ns/dcat#accessURL');
          const label = title || desc || access;
          const tr = domConstruct.create('tr', null, this.distributions);
          tdLabel = domConstruct.create('td', { innerHTML: label }, tr);
          tdButtons = domConstruct.create('td', null, tr);
          divWrapper = domConstruct.create('div', { class: 'min-height-row' }, tdButtons);
          infoWrapper = domConstruct.create('span', { class: 'badge' }, divWrapper);
          icon = domConstruct.create('span', { class: 'fa fa-info-circle' }, divWrapper);
          const f = lang.hitch(this, (ev) => {
            ev.stopPropagation();
            this.distributionInfoDialog.set('title', label);

            this.distributionInfoDialog.open(distE);
          });
          on(tr, 'click', f);
        }
      });
    },

    showResults(results) {
      if (results.length === 0) {
        domStyle.set(this.resultBlock, 'display', 'none');
        return;
      }
      domStyle.set(this.resultBlock, 'display', '');
      domAttr.set(this.results, 'innerHTML', '');
      results.forEach((resultE) => {
        if (resultE != null) {
          const md = resultE.getMetadata();
          const subj = resultE.getResourceURI();
          const title = md.findFirstValue(subj, 'dcterms:title');
          const desc = md.findFirstValue(subj, 'dcterms:description');
          const access = md.findFirstValue(subj, 'foaf:page');
          const label = title || desc || access;
          const tr = domConstruct.create('tr', null, this.results);
          domConstruct.create('td', { innerHTML: label }, tr);
          const tdButtons = domConstruct.create('td', null, tr);
          const infoButton = domConstruct.create('button', { class: 'btn btn-sm btn-default' }, tdButtons);
          domConstruct.create('span', { class: 'fa fa-info-circle' }, infoButton);
          const f = lang.hitch(this, (ev) => {
            ev.stopPropagation();
            this.infoDialog.title = label;
            this.infoDialog.localeChange();
            const dataResultTemplate = defaults.get('itemstore').getItem(config.catalog.datasetResultTemplateId);
            this.infoDialog.show(
              resultE.getResourceURI(), resultE.getMetadata(), dataResultTemplate);
          });
          on(infoButton, 'click', f);
          on(tr, 'click', f);
        }
      });
    },

    onCatalogClick(ev) {
      ev.stopPropagation();
      const catalogTemplate = defaults.get('itemstore').getItem(config.catalog.catalogTemplateId);
      const rdfutils = defaults.get('rdfutils');
      this.infoDialog.title = rdfutils.getLabel(this.dcatEntry);
      this.infoDialog.localeChange();
      this.infoDialog.show(
        this.dcatEntry.getResourceURI(),
        this.dcatEntry.getMetadata(),
        catalogTemplate);
    },
  }));
