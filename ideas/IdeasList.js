define([
  'dojo/_base/declare',
  '../defaults',
  'entryscape-commons/list/common/ETBaseList',
  'config',
  'entryscape-commons/list/common/ToggleRow',
  'dojo/dom-attr',
  'i18n!nls/escoList',
  'i18n!nls/escaIdeas',
], (declare, defaults, ETBaseList, config, ToggleRow, domAttr) => {
  const ns = defaults.get('namespaces');

  const IdeaRow = declare([ToggleRow], {

    nlsPublicTitle: 'publicResultTitle',
    nlsProtectedTitle: 'privateResultTitle',
    nlsContextSharingNoAccess: '',
    nlsConfirmRemoveRow: '',

    postCreate() {
      this.inherited('postCreate', arguments);
      this.nlsPublicTitle = this.list.nlsPublicTitle;
      this.nlsProtectedTitle = this.list.nlsProtectedTitle;
      this.nlsContextSharingNoAccess = '';
      this.nlsConfirmRemoveRow = this.list.nlsConfirmRemoveRow;
      this.entry.getContext().getEntry().then((contextEntry) => {
        this.catalogPublic = contextEntry.isPublic();
        this.setToggled(contextEntry.isPublic(), this.entry.isPublic());
      });
    },
    updateLocaleStrings(generic, specific) {
      this.inherited('updateLocaleStrings', arguments);
      if (!this.catalogPublic) {
        domAttr.set(this.protectedNode, 'title', specific.privateDisabledResultTitle);
      }
    },
    toggleImpl(onSuccess) {
      const ei = this.entry.getEntryInfo();
      const acl = ei.getACL(true);
      defaults.get('getGroupWithHomeContext')(this.entry.getContext()).then((groupEntry) => {
        if (this.isPublicToggle) {
          acl.admin = acl.admin || [];
          acl.admin.push(groupEntry.getId());
          ei.setACL(acl);
          ei.commit().then(onSuccess);
        } else {
          ei.setACL({});
          ei.commit().then(onSuccess);
        }
      });
    },
  });

  return declare([ETBaseList], {

    includeCreateButton: true,
    includeInfoButton: false,
    includeEditButton: true,
    includeRemoveButton: true,
    nlsBundles: ['escoList', 'escaIdeas'],
    entryType: ns.expand('esterms:Idea'),
    entitytype: 'datasetIdea',
    rowClass: IdeaRow,
    nlsPublicTitle: 'publicIdeaTitle',
    nlsProtectedTitle: 'privateIdeaTitle',
    nlsRemoveEntryConfirm: 'confirmRemoveIdea',
    searchVisibleFromStart: false,
    rowClickDialog: 'edit',

    postCreate() {
      this.inherited('postCreate', arguments);
    },

    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem(
          config.catalog.datasetIdeaTemplateId);
      }
      return this.template;
    },
    getSearchObject() {
      const context = defaults.get('context');
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      return es.newSolrQuery().rdfType(this.entryType).context(context.getResourceURI());
    },
  });
});
