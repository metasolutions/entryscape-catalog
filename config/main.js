define([
  'entryscape-commons/merge',
  'entryscape-catalog/config/catalogConfig',
], (merge, catalogConfig) => merge({
  theme: {
    appName: 'EntryScape Catalog',
    oneRowNavbar: true,
    localTheme: false,
  },
  locale: {
    fallback: 'en',
    supported: [
      { lang: 'de', flag: 'de', label: 'Deutsch', labelEn: 'German', shortDatePattern: 'dd. MMM' },
      { lang: 'en', flag: 'gb', label: 'English', labelEn: 'English', shortDatePattern: 'MMM dd' },
      { lang: 'sv', flag: 'se', label: 'Svenska', labelEn: 'Swedish', shortDatePattern: 'dd MMM' },
    ],
  },
  site: {
    siteClass: 'spa/Site',
    controlClass: 'entryscape-commons/nav/Layout',
    startView: 'signin',
    sidebar: { wide: false, always: true, replaceTabs: true },
    nationalIdNumber: 'swedishNationalNumber',
    views: {
      signin: {
        title: { en: 'Sign in/out', sv: 'Logga in/ut', de: 'An-/Abmelden' },
        class: 'entryscape-commons/nav/Signin',
        constructorParams: { nextView: 'cataloglist' },
      },
    },
  },
}, catalogConfig, __entryscape_config));
